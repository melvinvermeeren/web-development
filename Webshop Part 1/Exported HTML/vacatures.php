<!DOCTYPE HTML>
<html lang="nl" dir="ltr">
	<head>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
		<title>Lorenzo&apos;s Pizza - Vacatures</title>
		<link rel="shortcut icon" href="images/favicon.ico">
		<link rel="stylesheet" href="css/style.css">
	</head>
	<body>
		<aside id="sidebar_advertisement">
			<a href="webshop_producten.php"><img id="siderbar_advertisement_image" src="./images/sidebar.jpg" alt="Pizza Advertentie"/></a>		</aside>
		<div id="body">
			<header>
				<div id="header_user">
	<p>
		Inloggen
		<span class="header_user_hidden">
			<br/>
			<input type="text" size="25" placeholder="Gebruikersnaam"/>
			<input type="password" size="25" placeholder="Wachtwoord"/>
			<br/>
			Onthouden
			<input type="checkbox"/>
			<a href="wachtwoord_vergeten.php">Vergeten</a>
			<a href="registreren.php">Registreren</a>
			<input type="submit" value="Log in"/>
		</span>
	</p>
</div><a href="./"><img id="header_logo" src="images/header.png" alt="Webshop Logo"/></a>
<nav>
	<ul class="navigation">
		<li><a href="./">Nieuws</a></li>
		<li><a href="./acties.php">Acties</a></li>
		<li><a href="./overons.php">Over ons</a></li>
		<li><a href="./vacatures.php">Vacatures</a></li>
		<li class="navigation_sub"><a href="./webshop_producten.php">Webshop</a>
			<ul>
				<li><a href="./webshop_producten.php">Producten</a></li>
				<li><a href="./webshop_winkelwagen.php">Winkelwagen</a></li>
				<li><a href="./webshop_afrekenen.php">Afrekenen</a></li>
			</ul>
		</li>
	</ul>
</nav>			</header>
			<div class="text_padding">
				<div class="vacatures_vacature">
					<h1 class="vacatures_titel_vacature">
						Gezocht: Pizzabakker!
					</h1>
					<p class="vacatures_paragraaf_vacature">
						Gevraagd een assistent bakker op het gebied van steenovenpizza's.
						minimaal 2 jaar ervaring gevraagd op het gebied van steenovens.
					</p>
					<p class="vacatures_paragraaf_vacature">
						Aanstelling: 2 maanden proefperiode, daarna vaste aanstelling part time.
					<p class="vacatures_paragraaf_vacature">
						Uren per week:20
					</p>
					<p class="vacatures_paragraaf_vacature">
						Betaling per uur is te bespreken.
					</p>
					</div>
				<div class="vacatures_vacature">
					<h1 class="vacatures_titel_vacature">
						Gezocht: Privédetective!
					</h1>
					<p class="vacatures_paragraaf_vacature">
						Wij zijn onlangs opgelicht door W3Schools.
						Deze website gaf onjuiste informatie waardoor onze database corrupt was.
					</p>
					<p class="vacatures_paragraaf_vacature">
						Aanstelling: 2 maanden proefperiode, daarna vaste aanstelling full time.
					<p class="vacatures_paragraaf_vacature">
						Uren per week:70
					</p>
					<p class="vacatures_paragraaf_vacature">
						U krijgt als vergoeding gratis pizza&apos;s en pasta&apos;s.
					</p>
				</div>
			</div>
			<footer class="text_padding">
				&copy;2014 Melvin Vermeeren &amp; Jacob Siebelt.			</footer>
		</div>
	</body>
</html>