<!DOCTYPE HTML>
<html lang="nl" dir="ltr">
	<head>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
		<title>Lorenzo&apos;s Pizza - Nieuws</title>
		<link rel="shortcut icon" href="images/favicon.ico">
		<link rel="stylesheet" href="css/style.css">
	</head>
	<body>
		<aside id="sidebar_advertisement">
			<a href="webshop_producten.php"><img id="siderbar_advertisement_image" src="./images/sidebar.jpg" alt="Pizza Advertentie"/></a>		</aside>
		<div id="body">
			<header>
				<div id="header_user">
	<p>
		Inloggen
		<span class="header_user_hidden">
			<br/>
			<input type="text" size="25" placeholder="Gebruikersnaam"/>
			<input type="password" size="25" placeholder="Wachtwoord"/>
			<br/>
			Onthouden
			<input type="checkbox"/>
			<a href="wachtwoord_vergeten.php">Vergeten</a>
			<a href="registreren.php">Registreren</a>
			<input type="submit" value="Log in"/>
		</span>
	</p>
</div><a href="./"><img id="header_logo" src="images/header.png" alt="Webshop Logo"/></a>
<nav>
	<ul class="navigation">
		<li><a href="./">Nieuws</a></li>
		<li><a href="./acties.php">Acties</a></li>
		<li><a href="./overons.php">Over ons</a></li>
		<li><a href="./vacatures.php">Vacatures</a></li>
		<li class="navigation_sub"><a href="./webshop_producten.php">Webshop</a>
			<ul>
				<li><a href="./webshop_producten.php">Producten</a></li>
				<li><a href="./webshop_winkelwagen.php">Winkelwagen</a></li>
				<li><a href="./webshop_afrekenen.php">Afrekenen</a></li>
			</ul>
		</li>
	</ul>
</nav>			</header>

			<div class="text_padding">
				<h1 class="header_center">Nieuws</h1>
				<h2 class="nieuws_article_header">Pizza super aanbieding!</h2>
				<p class="nieuws_article_content">
					Dikke pizza&apos;s in de aanbieding, met ambachtelijk gerookte ansjovis.
					Zie <a href="./acties.php">acties</a> voor meer informatie.
				</p>
				<h2 class="nieuws_article_header">Website online!</h2>
				<p class="nieuws_article_content">
					Met trots presenteert Lorenzo&apos;s Pizza een fantastische website met cutting-edge design.
					HTML5 en CSS3 gecombineerd met PHP voor een moderne look en high-tech features!
				</p>
				<h2 class="nieuws_article_header">Gezakt!</h2>
				<p class="nieuws_article_content">
					Helaas ben ik gezakt voor mijn pizzadiploma, ik werk nu dus zwart mensen.
				</p>
			</div>

			<footer class="text_padding">
				&copy;2014 Melvin Vermeeren &amp; Jacob Siebelt.			</footer>
		</div>
	</body>
</html>
