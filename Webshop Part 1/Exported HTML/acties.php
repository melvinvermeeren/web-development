<!DOCTYPE HTML>
<html lang="nl" dir="ltr">
	<head>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
		<title>Lorenzo&apos;s Pizza - Acties</title>
		<link rel="shortcut icon" href="images/favicon.ico">
		<link rel="stylesheet" href="css/style.css">
	</head>
	<body>
		<aside id="sidebar_advertisement">
			<a href="webshop_producten.php"><img id="siderbar_advertisement_image" src="./images/sidebar.jpg" alt="Pizza Advertentie"/></a>		</aside>
		<div id="body">
			<header>
				<div id="header_user">
	<p>
		Inloggen
		<span class="header_user_hidden">
			<br/>
			<input type="text" size="25" placeholder="Gebruikersnaam"/>
			<input type="password" size="25" placeholder="Wachtwoord"/>
			<br/>
			Onthouden
			<input type="checkbox"/>
			<a href="wachtwoord_vergeten.php">Vergeten</a>
			<a href="registreren.php">Registreren</a>
			<input type="submit" value="Log in"/>
		</span>
	</p>
</div><a href="./"><img id="header_logo" src="images/header.png" alt="Webshop Logo"/></a>
<nav>
	<ul class="navigation">
		<li><a href="./">Nieuws</a></li>
		<li><a href="./acties.php">Acties</a></li>
		<li><a href="./overons.php">Over ons</a></li>
		<li><a href="./vacatures.php">Vacatures</a></li>
		<li class="navigation_sub"><a href="./webshop_producten.php">Webshop</a>
			<ul>
				<li><a href="./webshop_producten.php">Producten</a></li>
				<li><a href="./webshop_winkelwagen.php">Winkelwagen</a></li>
				<li><a href="./webshop_afrekenen.php">Afrekenen</a></li>
			</ul>
		</li>
	</ul>
</nav>			</header>

			<div class="text_padding">
				<div>
					<h1 class="header_center">Acties</h1>
					<p>
						Deze <em>ambachtelijke</em> pizza is in de aanbieding.
						Gemaakt door echte italiaanse vakmensen bereid in een hand-gemaakte steenoven met heerlijke vis.
						Koop vandaag bij <i>Lorenzo&apos;s Pizza</i>!
					</p>
					<div class="webshop_producten_product">
						<a href="./productpagina.php"><img class="webshop_producten_product_image" src="images/products/pizza_european_anchovy.jpg" alt="Pizza met ansjovis"/></a>
						<p>Pizza met ansjovis</p>
						<p><span class="product_discount">&euro;24,95</span> &euro;19,95 <input type="submit" value="In winkelwagen" onclick='location.href="./webshop_winkelwagen.php"'/></p>
					</div>
				</div>
			</div>

			<footer class="text_padding">
				&copy;2014 Melvin Vermeeren &amp; Jacob Siebelt.			</footer>
		</div>
	</body>
</html>
