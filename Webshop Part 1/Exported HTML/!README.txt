To use this move all the files UP one folder (to "Webshop Part 1").

The links point to .php files, that's why these are .php files even though they're actually pure HTML.

--- Notes ---

The W3C Validation Service rejects .php files, there is an archive with .html file extensions for your convenience.

Every link pointing to the index doesn't work since it redirects to "href=./" instead of "href=./index.php" since that looks nicer in the browser when the files are actually on a server.