<!DOCTYPE HTML>
<html lang="nl" dir="ltr">
	<head>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
		<title>Lorenzo&apos;s Pizza - Webshop winkelwagen</title>
		<link rel="shortcut icon" href="images/favicon.ico">
		<link rel="stylesheet" href="css/style.css">
	</head>
	<body>
		<aside id="sidebar_advertisement">
			<?php include 'pages/sidebar.php'; ?>
		</aside>
		<div id="body">
			<header>
				<?php include 'pages/header.php'; ?>
			</header>

			<div class="text_padding">
				<input type="button" value="Verder winkelen" onclick='location.href="./webshop_producten.php"'/>
				<h1 id="webshop_winkelwagen_header">Winkelwagen</h1>
				<table id="webshop_winkelwagen_table_main" class="webshop_winkelwagen_table">
					<tr>
						<th>Productfoto</th>
						<th>Productnaam</th>
						<th>Prijs</th>
						<th>Aantal</th>
						<th>Subtotaal</th>
						<th>Verwijderen</th>
					</tr>
					<tr>
						<td class="webshop_winkelwagen_table_images"><a href="./productpagina.php"><img class="webshop_winkelwagen_table_images_image" src="images/products/pasta_spaghetti_crab.jpg" alt="Spaghetti met krab"/></a></td>
						<td class="webshop_winkelwagen_table_text">Spaghetti met krab</td>
						<td class="webshop_winkelwagen_table_text">&euro;19,95</td>
						<td class="webshop_winkelwagen_table_text"><input type="text" value="2" size="5"/></td>
						<td class="webshop_winkelwagen_table_text">&euro;39,90</td>
						<td class="webshop_winkelwagen_table_text"><a href="#"><div class="webshop_winkelwagen_table_remove"></div></a></td>
					</tr>
					<tr>
						<td class="webshop_winkelwagen_table_images"><a href="./productpagina.php"><img class="webshop_winkelwagen_table_images_image" src="images/products/pasta_vegatables.jpg" alt="Pasta met groentes"/></a></td>
						<td class="webshop_winkelwagen_table_text">Pasta met groentes</td>
						<td class="webshop_winkelwagen_table_text">&euro;12,50</td>
						<td class="webshop_winkelwagen_table_text"><input type="text" value="3" size="5"/></td>
						<td class="webshop_winkelwagen_table_text">&euro;37,50</td>
						<td class="webshop_winkelwagen_table_text"><a href="#"><div class="webshop_winkelwagen_table_remove"></div></a></td>
					</tr>
				</table>
				<div style="text-align: right;">
					<input type="button" value="Herberekenen bedrag"/>
					<table id="webshop_winkelwagen_table_total" class="webshop_winkelwagen_table">
						<tr>
							<th>Eindtotaal</th>
							<td class="webshop_winkelwagen_table_text">&euro;77,40</td>
						</tr>
					</table>
					<br/>
					<input type="button" value="Afrekenen" onclick='location.href="./webshop_afrekenen.php"'/>
				</div>
			</div>
			<footer class="text_padding">
				<?php include 'pages/footer.php'; ?>
			</footer>
		</div>
	</body>
</html>
