<nav>
	<ul class="navigation">
		<li><a href="./">Nieuws</a></li>
		<li><a href="./acties.php">Acties</a></li>
		<li><a href="./overons.php">Over ons</a></li>
		<li><a href="./vacatures.php">Vacatures</a></li>
		<li class="navigation_sub"><a href="./webshop_producten.php">Webshop</a>
			<ul>
				<li><a href="./webshop_producten.php">Producten</a></li>
				<li><a href="./webshop_winkelwagen.php">Winkelwagen</a></li>
				<li><a href="./webshop_afrekenen.php">Afrekenen</a></li>
			</ul>
		</li>
	</ul>
</nav>