<!DOCTYPE HTML>
<html lang="nl" dir="ltr">
	<head>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
		<title>Lorenzo&apos;s Pizza - Registreren</title>
		<link rel="shortcut icon" href="images/favicon.ico">
		<link rel="stylesheet" href="css/style.css">
	</head>
	<body>
		<aside id="sidebar_advertisement">
			<?php include 'pages/sidebar.php'; ?>
		</aside>
		<div id="body">
			<header>
				<?php include 'pages/header.php'; ?>
			</header>

			<div class="text_padding">
				<h1 class="header_center">
					Registreren
				</h1>
				<div>
					<p>
						Vul uw gegevens in:
					</p>
				</div>
				<div id="registreren_invoerlijst_left">
					<h2 style="padding-left: 1%; margin-top:0px;">
						Accountgegevens:
					</h2>
					<div class="registreren_accountgegevens">
						Gebruikersnaam*
						<br/>
						<input type="text" name="input" size="49"/>
					</div>
					<div class="registreren_accountgegevens">
						Wachtwoord*
						<br/>
						<input type="text" name="input" size="49"/>
					</div>
					<div class="registreren_accountgegevens">
						HerhaalWachtwoord*
						<br/>
						<input type="text" name="input" size="49"/>
					</div>
				</div>
				<div id="registreren_invoerlijst_right">
					<h2 style="padding-left: 1%; margin-top: 0px;">
						Factuuradres:
					</h2>
					<div  id="registreren_aanhef">
						Aanhef*
						<br/>
						<select>
							<option value="Dhr" selected>
								Dhr.
							</option>
							<option value="Mev">
								Mev.
							</option>
						</select>
					</div>
					<div class="registreren_voornaamtussenv">
						Voornaam*
						<br/>
						<input type="text" name="input"/>
					</div>
					<div class="registreren_voornaamtussenv">
						Tussenvoegsels*
						<br/>
						<input type="text" name="input" size="5"/>
					</div>
					<div id="registreren_achternaam">
						Achternaam*
						<br/>
						<input type="text" name="input" size="28"/>
					</div>
					<div id="registreren_email">
						E-mailadres*
						<br/>
						<input type="text" name="input" size="110"/>
					</div>
					<div class="registreren_strtnaampostcode">
						Straatnaam*
						<br/>
						<input type="text" name="input" size="50"/>
					</div>
					<div class="registreren_huisnummerplaatsnaamtelefoon">
						Huisnummer*
						<br/>
						<input type="text" name="input" size="50"/>
					</div>
					<div class="registreren_strtnaampostcode">
						Postcode*
						<br/>
						<input type="text" name="input" size="50"/>
					</div>
					<div class="registreren_huisnummerplaatsnaamtelefoon">
						Plaatsnaam*
						<br/>
						<input type="text" name="input" size="50"/>
					</div>
					<div class="registreren_huisnummerplaatsnaamtelefoon">
						Telefoon*
						<br/>
						<input type="text" name="input" size="50"/>
					</div>
				</div>
				<div id="registreren_submit_button">
					<input type="submit" value="Opslaan">
				</div>
			</div>
			<footer class="text_padding">
				<?php include 'pages/footer.php'; ?>
			</footer>
		</div>
	</body>
</html>