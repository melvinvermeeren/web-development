<!DOCTYPE HTML>
<html lang="nl" dir="ltr">
	<head>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
		<title>Lorenzo&apos;s Pizza</title>
		<link rel="shortcut icon" href="images/favicon.ico">
		<link rel="stylesheet" href="css/style.css">
	</head>
	<body>
		<aside id="sidebar_advertisement">
			<?php include 'pages/sidebar.php'; ?>
		</aside>
		<div id="body">
			<header>
				<?php include 'pages/header.php'; ?>
			</header>
			<div class="text_padding">
				<h1>
					Afrekenen
				</h1>
				Kies de bank waarmee u wilt betalen:
				<select>
					<option value="Kies uw bank" selected>
						Kies uw bank
					</option>
					<option value="Rabobank">
						Rabobank
					</option>
					<option value="ABN Amro">
						ABN Amro
					</option>
				</select>
				<p>
					<input type="checkbox"/>Ik accepteer de <a href="#">Gebruikersvoorwaarden</a>.
				</p>
				<p>
					Ga door naar de betaalpagina van uw bank.
					<input type="submit" value="Annuleren" onclick='location.href="./webshop_winkelwagen.php"'/>
					<input type="submit" value="Betalen" onclick="alert('Hah, je bent opgelicht!')"/>
				</p>
			</div>
			<footer class="text_padding">
				<?php include 'pages/footer.php'; ?>
			</footer>
		</div>
	</body>
</html>
