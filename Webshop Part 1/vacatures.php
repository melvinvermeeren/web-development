<!DOCTYPE HTML>
<html lang="nl" dir="ltr">
	<head>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
		<title>Lorenzo&apos;s Pizza - Vacatures</title>
		<link rel="shortcut icon" href="images/favicon.ico">
		<link rel="stylesheet" href="css/style.css">
	</head>
	<body>
		<aside id="sidebar_advertisement">
			<?php include 'pages/sidebar.php'; ?>
		</aside>
		<div id="body">
			<header>
				<?php include 'pages/header.php'; ?>
			</header>
			<div class="text_padding">
				<div class="vacatures_vacature">
					<h1 class="vacatures_titel_vacature">
						Gezocht: Pizzabakker!
					</h1>
					<p class="vacatures_paragraaf_vacature">
						Gevraagd een assistent bakker op het gebied van steenovenpizza's.
						minimaal 2 jaar ervaring gevraagd op het gebied van steenovens.
					</p>
					<p class="vacatures_paragraaf_vacature">
						Aanstelling: 2 maanden proefperiode, daarna vaste aanstelling part time.
					<p class="vacatures_paragraaf_vacature">
						Uren per week:20
					</p>
					<p class="vacatures_paragraaf_vacature">
						Betaling per uur is te bespreken.
					</p>
					</div>
				<div class="vacatures_vacature">
					<h1 class="vacatures_titel_vacature">
						Gezocht: Privédetective!
					</h1>
					<p class="vacatures_paragraaf_vacature">
						Wij zijn onlangs opgelicht door W3Schools.
						Deze website gaf onjuiste informatie waardoor onze database corrupt was.
					</p>
					<p class="vacatures_paragraaf_vacature">
						Aanstelling: 2 maanden proefperiode, daarna vaste aanstelling full time.
					<p class="vacatures_paragraaf_vacature">
						Uren per week:70
					</p>
					<p class="vacatures_paragraaf_vacature">
						U krijgt als vergoeding gratis pizza&apos;s en pasta&apos;s.
					</p>
				</div>
			</div>
			<footer class="text_padding">
				<?php include 'pages/footer.php'; ?>
			</footer>
		</div>
	</body>
</html>