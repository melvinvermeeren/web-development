<!DOCTYPE HTML>
<html lang="nl" dir="ltr">
	<head>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
		<title>Lorenzo&apos;s Pizza - Acties</title>
		<link rel="shortcut icon" href="images/favicon.ico">
		<link rel="stylesheet" href="css/style.css">
	</head>
	<body>
		<aside id="sidebar_advertisement">
			<?php include 'pages/sidebar.php'; ?>
		</aside>
		<div id="body">
			<header>
				<?php include 'pages/header.php'; ?>
			</header>

			<div class="text_padding">
				<div>
					<h1 class="header_center">Acties</h1>
					<p>
						Deze <em>ambachtelijke</em> pizza is in de aanbieding.
						Gemaakt door echte italiaanse vakmensen bereid in een hand-gemaakte steenoven met heerlijke vis.
						Koop vandaag bij <i>Lorenzo&apos;s Pizza</i>!
					</p>
					<div class="webshop_producten_product">
						<a href="./productpagina.php"><img class="webshop_producten_product_image" src="images/products/pizza_european_anchovy.jpg" alt="Pizza met ansjovis"/></a>
						<p>Pizza met ansjovis</p>
						<p><span class="product_discount">&euro;24,95</span> &euro;19,95 <input type="submit" value="In winkelwagen" onclick='location.href="./webshop_winkelwagen.php"'/></p>
					</div>
				</div>
			</div>

			<footer class="text_padding">
				<?php include 'pages/footer.php'; ?>
			</footer>
		</div>
	</body>
</html>
