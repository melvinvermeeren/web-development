<!DOCTYPE HTML>
<html lang="nl" dir="ltr">
	<head>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
		<title>Lorenzo&apos;s Pizza - Webshop producten</title>
		<link rel="shortcut icon" href="images/favicon.ico">
		<link rel="stylesheet" href="css/style.css">
	</head>
	<body>
		<aside id="sidebar_advertisement">
			<?php include 'pages/sidebar.php'; ?>
		</aside>
		<div id="body">
			<header>
				<?php include 'pages/header.php'; ?>
			</header>

			<div class="text_padding">
				<div id="webshop_producten_search">
			 		<input type="text" value=""/>
			 		<input type="submit" value="Zoek"/>
				</div>
				<p style="margin: 0px;">
					<select>
						<option value="all" selected>Alle Gerechten</option>
						<option value="pizza">Pizza</option>
						<option value="pasta">Pasta</option>
					</select>
					<select>
						<option value="all" selected>Alle Pizza&apos;s</option>
						<option value="stoneoven">Steenoven</option>
						<option value="vegetarian">Vegatarisch</option>
						<option value="turkish">Turks</option>
					</select>
					83 producten | Toon
					<select>
						<option value="20" selected>20</option>
						<option value="50">50</option>
						<option value="100">100</option>
					</select>
					per pagina.
				</p>
				<br>
				<div id="webshop_producten_products">
					<div class="webshop_producten_product">
						<a href="./productpagina.php"><img class="webshop_producten_product_image" src="images/products/pasta_spaghetti_bolognese.jpg" alt="Spaghetti Bolognese"/></a>
						<p>Spaghetti Bolognese</p>
						<p>&euro;14,95 <input type="submit" value="In winkelwagen" onclick='location.href="./webshop_winkelwagen.php"'/></p>
					</div>
					<div class="webshop_producten_product">
						<a href="./productpagina.php"><img class="webshop_producten_product_image" src="images/products/pasta_spaghetti_crab.jpg" alt="Spaghetti met krab"/></a>
						<p>Spaghetti met krab</p>
						<p>&euro;19,95 <input type="submit" value="In winkelwagen" onclick='location.href="./webshop_winkelwagen.php"'/></p>
					</div>
					<div class="webshop_producten_product">
						<a href="./productpagina.php"><img class="webshop_producten_product_image" src="images/products/pasta_spaghetti_meatballs.jpg" alt="Spaghetti met gehaktballen"/></a>
						<p>Spaghetti met gehaktballen</p>
						<p>&euro;17,50 <input type="submit" value="In winkelwagen" onclick='location.href="./webshop_winkelwagen.php"'/></p>
					</div>
					<div class="webshop_producten_product">
						<a href="./productpagina.php"><img class="webshop_producten_product_image" src="images/products/pasta_vegatables.jpg" alt="Pasta met groentes"/></a>
						<p>Pasta met groentes</p>
						<p>&euro;12,50 <input type="submit" value="In winkelwagen" onclick='location.href="./webshop_winkelwagen.php"'/></p>
					</div>
					<div class="webshop_producten_product">
						<a href="./productpagina.php"><img class="webshop_producten_product_image" src="images/products/pizza_european_anchovy.jpg" alt="Pizza met ansjovis"/></a>
						<p>Pizza met ansjovis</p>
						<p><span class="product_discount">&euro;24,95</span> &euro;19,95 <input type="submit" value="In winkelwagen" onclick='location.href="./webshop_winkelwagen.php"'/></p>
					</div>
					<div class="webshop_producten_product">
						<a href="./productpagina.php"><img class="webshop_producten_product_image" src="images/products/pizza_hawaiian.jpg" alt="Pizza Hawaii"/></a>
						<p>Pizza Hawaii</p>
						<p>&euro;15,95 <input type="submit" value="In winkelwagen" onclick='location.href="./webshop_winkelwagen.php"'/></p>
					</div>
					<div class="webshop_producten_product">
						<a href="./productpagina.php"><img class="webshop_producten_product_image" src="images/products/pizza_lahmacun.jpg" alt="Pizza Lahmacun"/></a>
						<p>Pizza Lahmacun</p>
						<p>&euro;22,50 <input type="submit" value="In winkelwagen" onclick='location.href="./webshop_winkelwagen.php"'/></p>
					</div>
					<div class="webshop_producten_product">
						<a href="./productpagina.php"><img class="webshop_producten_product_image" src="images/products/pizza_salami.jpg" alt="Pizza Salami"/></a>
						<p>Pizza Salami</p>
						<p>&euro;19,95 <input type="submit" value="In winkelwagen" onclick='location.href="./webshop_winkelwagen.php"'/></p>
					</div>
					<div class="webshop_producten_product">
						<a href="./productpagina.php"><img class="webshop_producten_product_image" src="images/products/pizza_special.jpg" alt="Pizza Special"/></a>
						<p>Pizza Special</p>
						<p>&euro;29,95 <input type="submit" value="In winkelwagen" onclick='location.href="./webshop_winkelwagen.php"'/></p>
					</div>
				</div>
			</div>

			<footer class="text_padding">
				<?php include 'pages/footer.php'; ?>
			</footer>
		</div>
	</body>
</html>
