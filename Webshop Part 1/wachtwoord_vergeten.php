<!DOCTYPE HTML>
<html lang="nl" dir="ltr">
	<head>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
		<title>Lorenzo&apos;s Pizza - Wachtwoord vergeten</title>
		<link rel="shortcut icon" href="images/favicon.ico">
		<link rel="stylesheet" href="css/style.css">
	</head>
	<body>
		<aside id="sidebar_advertisement">
			<?php include 'pages/sidebar.php'; ?>
		</aside>
		<div id="body">
			<header>
				<?php include 'pages/header.php'; ?>
			</header>

			<div class="text_padding">
				<h1 class="header_center">Wachtwoord vergeten</h1>
				<p id="wachtwoord_vergeten">
					<input type="text" size="25" placeholder="Gebruikersnaam"/>
					of
					<input class="input_email" type="email" size="25" placeholder="Emailadres"/>
					<input type="submit" value="Versturen" onclick='location.href="#"'/>
				</p>
			</div>

			<footer class="text_padding">
				<?php include 'pages/footer.php'; ?>
			</footer>
		</div>
	</body>
</html>
