<!DOCTYPE HTML>
<html lang="nl" dir="ltr">
	<head>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
		<title>Lorenzo&apos;s Pizza - Webshop afrekenen</title>
		<link rel="shortcut icon" href="images/favicon.ico">
		<link rel="stylesheet" href="css/style.css">
	</head>
	<body>
		<aside id="sidebar_advertisement">
			<?php include 'pages/sidebar.php'; ?>
		</aside>
		<div id="body">
			<header>
				<?php include 'pages/header.php'; ?>
			</header>
			<div class="text_padding">
				<div id="product_highlight">
						<h1 id="product_highlight_title">
							Spagetti Bolognese
						</h1>
						<img id="product_big_image" src="images/products/pasta_spaghetti_bolognese.jpg" alt="Spaghetti Bolognese"/>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec a diam lectus. Sed sit amet ipsum mauris. Maecenas congue ligula ac quam viverra nec consectetur ante hendrerit. Donec et mollis dolor. Praesent et diam eget libero egestas mattis sit amet vitae augue. Nam tincidunt congue enim, ut porta lorem lacinia consectetur. Donec ut libero sed arcu vehicula ultricies a non tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ut gravida lorem. Ut turpis felis, pulvinar a semper sed, adipiscing id dolor. Pellentesque auctor nisi id magna consequat sagittis. Curabitur dapibus enim sit amet elit pharetra tincidunt feugiat nisl imperdiet. Ut convallis libero in urna ultrices accumsan. Donec sed odio eros. Donec viverra mi quis quam pulvinar at malesuada arcu rhoncus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. In rutrum accumsan ultricies. Mauris vitae nisi at sem facilisis semper ac in est.
					<br />
					<div id="product_highlight_options">
						<span class="product_highlight_span">
							Op voorraad: 13.
						</span>
						<span class="product_highlight_span">
							&euro;14,95
						</span>
						<span class="product_highlight_span">
							Aantal:<input type="text" size="5" value="1"/>
						</span>
						<span class="product_highlight_span">
							<input type="submit" value="In winkelwagen" onclick='location.href="./webshop_winkelwagen.php"'/>
						</span>
					</div>
				</div>
				<div id="product_moreproducts">
					<div class="webshop_producten_product">
						<a href="./productpagina.php"><img class="webshop_producten_product_image" src="images/products/pasta_spaghetti_crab.jpg" alt="Spaghetti met krab"/></a>
						<p>Spaghetti met krab</p>
						<p>&euro;19,95 <input type="submit" value="In winkelwagen" onclick='location.href="./webshop_winkelwagen.php"'/></p>
					</div>
					<div class="webshop_producten_product">
						<a href="./productpagina.php"><img class="webshop_producten_product_image" src="images/products/pasta_spaghetti_meatballs.jpg" alt="Spaghetti met gehaktballen"/></a>
						<p>Spaghetti met gehaktballen</p>
						<p>&euro;17,50 <input type="submit" value="In winkelwagen" onclick='location.href="./webshop_winkelwagen.php"'/></p>
					</div>
					<div class="webshop_producten_product">
						<a href="./productpagina.php"><img class="webshop_producten_product_image" src="images/products/pasta_vegatables.jpg" alt="Pasta met groentes"/></a>
						<p>Pasta met groentes</p>
						<p>&euro;12,50 <input type="submit" value="In winkelwagen" onclick='location.href="./webshop_winkelwagen.php"'/></p>
					</div>
				</div>
			</div>
			<footer class="text_padding">
				<?php include 'pages/footer.php'; ?>
			</footer>
		</div>
	</body>
</html>
