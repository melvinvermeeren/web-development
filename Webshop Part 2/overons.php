<?php
	/**
	 * ©2014 Melvin Vermeeren & Jacob Siebelt. All rights reserved.
	 */
	session_start();
	include('php/functions.php');
?>

<!DOCTYPE HTML>
<html lang="nl" dir="ltr">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
	<title>Lorenzo&apos;s Pizza - Over ons</title>
	<link rel="shortcut icon" href="images/favicon.ico">
	<link rel="stylesheet" href="css/style.css">
</head>
<body>
<aside id="sidebar_advertisement">
	<?php include 'pages/sidebar.php'; ?>
</aside>
<div id="body">
	<header>
		<?php include 'pages/header.php'; ?>
	</header>

	<div id="overons" class="text_padding">
		<h1>Wie zijn wij</h1>
		<img id="overons_restaurant_interior" class="image_right" src="images/restaurant_interior.jpg"
		     alt="Interieur restaurant"/>

		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec a diam lectus. Sed sit amet ipsum mauris.
		   Maecenas congue ligula ac quam viverra nec consectetur ante hendrerit. Donec et mollis dolor. Praesent et
		   diam eget libero egestas mattis sit amet vitae augue. Nam tincidunt congue enim, ut porta lorem lacinia
		   consectetur. Donec ut libero sed arcu vehicula ultricies a non tortor. Lorem ipsum dolor sit amet,
		   consectetur adipiscing elit. Aenean ut gravida lorem. Ut turpis felis, pulvinar a semper sed, adipiscing id
		   dolor. Pellentesque auctor nisi id magna consequat sagittis. Curabitur dapibus enim sit amet elit pharetra
		   tincidunt feugiat nisl imperdiet. Ut convallis libero in urna ultrices accumsan. Donec sed odio eros. Donec
		   viverra mi quis quam pulvinar at malesuada arcu rhoncus. Cum sociis natoque penatibus et magnis dis
		   parturient montes, nascetur ridiculus mus. In rutrum accumsan ultricies. Mauris vitae nisi at sem facilisis
		   semper ac in est.</p>

		<h1>Routebeschrijving</h1>
		<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false"></script>
		<div id="overons_map" class="image_right">
			<div id="gmap_canvas" style="height:100%;width:100%;"></div>
		</div>
		<script type="text/javascript"> function init_map() {
				var myOptions = {zoom: 14, center: new google.maps.LatLng(51.826126, 5.864081199999987), mapTypeId: google.maps.MapTypeId.ROADMAP};
				map = new google.maps.Map(document.getElementById("gmap_canvas"), myOptions);
				marker = new google.maps.Marker({map: map, position: new google.maps.LatLng(51.826126, 5.864081199999987)});
				infowindow = new google.maps.InfoWindow({content: "<div style='position:relative;line-height:1.34;overflow:hidden;white-space:nowrap;display:block;'><div style='margin-bottom:2px;font-weight:500;'>Hogeschool van Arnhem en Nijmegen</div><span>Kapittelweg 33 <br>  Nijmegen</span></div>" });
				google.maps.event.addListener(marker, "click", function () {
					infowindow.open(map, marker);
				});
				infowindow.open(map, marker);
			}
			google.maps.event.addDomListener(window, 'load', init_map);</script>
		<p>Vivamus fermentum semper porta. Nunc diam velit, adipiscing ut tristique vitae, sagittis vel odio. Maecenas
		   convallis ullamcorper ultricies. Curabitur ornare, ligula semper consectetur sagittis, nisi diam iaculis
		   velit, id fringilla sem nunc vel mi. Nam dictum, odio nec pretium volutpat, arcu ante placerat erat, non
		   tristique elit urna et turpis. Quisque mi metus, ornare sit amet fermentum et, tincidunt et orci. Fusce eget
		   orci a orci congue vestibulum. Ut dolor diam, elementum et vestibulum eu, porttitor vel elit. Curabitur
		   venenatis pulvinar tellus gravida ornare. Sed et erat faucibus nunc euismod ultricies ut id justo. Nullam
		   cursus suscipit nisi, et ultrices justo sodales nec. Fusce venenatis facilisis lectus ac semper. Aliquam at
		   massa ipsum. Quisque bibendum purus convallis nulla ultrices ultricies. Nullam aliquam, mi eu aliquam
		   tincidunt, purus velit laoreet tortor, viverra pretium nisi quam vitae mi. Fusce vel volutpat elit. Nam
		   sagittis nisi dui.</p>
	</div>

	<footer class="text_padding">
		<?php include 'pages/footer.php'; ?>
	</footer>
</div>
</body>
</html>
