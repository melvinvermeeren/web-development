<?php
	/**
	 * ©2014 Melvin Vermeeren & Jacob Siebelt. All rights reserved.
	 */
	session_start();
	include('php/functions.php');
?>

<!DOCTYPE HTML>
<html lang="nl" dir="ltr">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
	<title>Lorenzo&apos;s Pizza - Nieuws</title>
	<link rel="shortcut icon" href="images/favicon.ico">
	<link rel="stylesheet" href="css/style.css">
</head>
<body>
<aside id="sidebar_advertisement">
	<?php include 'pages/sidebar.php'; ?>
</aside>
<div id="body">
	<header>
		<?php include 'pages/header.php'; ?>
	</header>

	<div class="text_padding">
		<h1 class="header_center">Nieuws</h1>

		<h2 class="nieuws_article_header">Pizza super aanbieding!</h2>

		<p class="nieuws_article_content">
			Dikke pizza&apos;s in de aanbieding, met ambachtelijk gerookte ansjovis.
			Zie <a href="./acties.php">acties</a> voor meer informatie.
		</p>

		<h2 class="nieuws_article_header">Website online!</h2>

		<p class="nieuws_article_content">
			Met trots presenteert Lorenzo&apos;s Pizza een fantastische website met cutting-edge design.
			HTML5 en CSS3 gecombineerd met PHP voor een moderne look en high-tech features!
		</p>

		<h2 class="nieuws_article_header">Gezakt!</h2>

		<p class="nieuws_article_content">
			Helaas ben ik gezakt voor mijn pizzadiploma, ik werk nu dus zwart mensen.
		</p>
	</div>

	<footer class="text_padding">
		<?php include 'pages/footer.php'; ?>
	</footer>
</div>
</body>
</html>
