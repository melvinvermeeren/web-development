<?php
	/**
	 * ©2014 Melvin Vermeeren & Jacob Siebelt. All rights reserved.
	 */
	session_start();
	include('php/functions.php');
	database_openConnection($conn);

	$username = ((array_key_exists('username', $_SESSION)) ? ($_SESSION['username']) : (''));
	$paid = ((array_key_exists('paid', $_POST)) ? true : false);
	$stockErrorGoToWinkelwagen = false;
	$cartProductCount = 0;

	if ($username == isUserLoggedIn())
	{
		$lookInCart = sqlsrv_query(
				$conn,
				"select * from [cart] where [username] = '$username'",
				array(), array("Scrollable" => SQLSRV_CURSOR_KEYSET));
		if ($lookInCart === false) exit('<pre>' . print_r(sqlsrv_errors(), true) . '</pre>');

		$cartProductCount = sqlsrv_num_rows($lookInCart);
		if ($cartProductCount === false) exit('<pre>' . print_r(sqlsrv_errors(), true) . '</pre>');

		if (array_key_exists('acceptedTOS', $_POST) && $cartProductCount > 0 && $paid)
		{
			$quantityAndNumberQuery = sqlsrv_query($conn, "select [number], [quantity] from [cart] where [username] = '$username'");
			if ($quantityAndNumberQuery === false) exit('<pre>' . print_r(sqlsrv_errors(), true) . '</pre>');

			$quantityAndNumberQueryClone = sqlsrv_query($conn, "select [number], [quantity] from [cart] where [username] = '$username'");
			if ($quantityAndNumberQueryClone === false) exit('<pre>' . print_r(sqlsrv_errors(), true) . '</pre>');

			$productQuery = sqlsrv_query(
					$conn,
					"select [number], [stock] from [product] where [number] in
			(select [number] from [cart] where [username] = '$username')",
					array(), array("Scrollable" => SQLSRV_CURSOR_KEYSET));
			if ($productQuery === false) exit('<pre>' . print_r(sqlsrv_errors(), true) . '</pre>');

			while ($row = sqlsrv_fetch_array($quantityAndNumberQuery))
			{
				while ($rowClone = sqlsrv_fetch_array($quantityAndNumberQueryClone))
				{
					$rowTwo = sqlsrv_fetch_array($productQuery);
					if ($rowClone['quantity'] > $rowTwo['stock'])
					{
						$stockErrorProductNumber   = $rowTwo['number'];
						$stockErrorGoToWinkelwagen = true;
						break;
					}
				}
				if ($stockErrorGoToWinkelwagen) break;


				$updateStockQuery = sqlsrv_query(
						$conn,
						"update [product] set [stock] -= '" . $row['quantity'] . "' where [number] = '" . $row['number'] . "'");
				if ($updateStockQuery === false) exit('<pre>' . print_r(sqlsrv_errors(), true) . '</pre>');

				$emptyCartQuery = sqlsrv_query(
						$conn,
						"delete from [cart] where [username] = '$username'");
				if ($emptyCartQuery === false) exit('<pre>' . print_r(sqlsrv_errors(), true) . '</pre>');
			}
			sqlsrv_free_stmt($quantityAndNumberQueryClone);
			unset($quantityAndNumberQueryClone);
		}
	}
?>

	<!DOCTYPE HTML>
	<html lang="nl" dir="ltr">
	<head>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
		<title>Lorenzo&apos;s Pizza - Webshop afrekenen</title>
		<link rel="shortcut icon" href="images/favicon.ico">
		<link rel="stylesheet" href="css/style.css">
	</head>
	<body>
	<?php
		if ($cartProductCount != 0 && $stockErrorGoToWinkelwagen)
		{
			database_closeConnection($conn);
			echo('<form action="webshop_winkelwagen.php" method="post" name="frmStockError">
					<input type="hidden" name="stockError" value="true"/>
					<input type="hidden" name="stockErrorProductNumber" value="' . $stockErrorProductNumber . '"/>
					</form>
					<script type="text/javascript">document.frmStockError.submit();</script>
					</body></html>');
			exit();
		}
	?>
	<aside id="sidebar_advertisement">
		<?php include 'pages/sidebar.php'; ?>
	</aside>
	<div id="body">
		<header>
			<?php include 'pages/header.php'; ?>
		</header>
		<div class="text_padding">
			<?php
				if (!isUserLoggedIn())
				{
					?>
					<div class="info_box">
						<h2>U bent niet ingelogd.</h2>

						<p>
							U moet ingelogd zijn om te kunnen afrekenen.<br/>
							Log rechtsboven op de pagina in of registreer.<br/>
							<input type="button" value="Registreren" onclick="location.href='registreren.php'"/>
							<br/>
						</p>
					</div>
				<?php
				}

				else if ($cartProductCount == 0)
				{
					?>
					<div class="info_box">
						<h2>U heeft niks in uw winkelwagen zitten.</h2>

						<p>
							Voeg eerst wat aan uw winkelwagen toe.<br/>
							Uw bestelling kan dan worden afgehandeld.
						</p>
						<input type="button" value="Naar webshop" onclick="location.href='webshop_producten.php'"/>
						<br/>
						<br/>
					</div>
				<?php
				}

				else if (array_key_exists('acceptedTOS', $_POST) && $paid)
				{
					?>
					<div class="success_box">
						<h2>Uw betaling is succesvol afgerond.</h2>

						<p>
							Bedankt voor uw bestelling.<br/>
							Uw bestelling wordt spoedig thuis bezorgd.
						</p>
						<input type="button" value="Naar webshop" onclick="location.href='webshop_producten.php'"/>
						<br/>
						<br/>
					</div>
				<?php
				}
				else
				{
					if (!array_key_exists('acceptedTOS', $_POST) && $paid)
					{
						?>
						<div class="error_box">
							<h2>U moet het de gebruikersvoorwaarden accepteren.</h2>

							<p>
								U kunt geen bestelling doen als u de gebruikersvoorwaarden niet accepteert.<br/>
								Als u het er niet mee eens bent kunt u de bestelling anuleren.
							</p>
						</div>
					<?php
					}
					?>
					<h1>
						Afrekenen
					</h1>
					Kies de bank waarmee u wilt betalen:
					<select>
						<option selected>ABN Amro</option>
						<option>Achmea</option>
						<option>AEGON</option>
						<option>Alex</option>
						<option>ASN Bank</option>
						<option>Delta Lloyd Bank</option>
						<option>ING Bank</option>
						<option>Van Lanschot</option>
						<option>Rabobank</option>
						<option>RegioBank</option>
						<option>SNS Bank</option>
						<option>Royal Bank of Scotland</option>
					</select>

					<form action="webshop_afrekenen.php" method="post">
						<p>
							<label>
								<input type="checkbox" name="acceptedTOS" value="true"/>
							</label>
							Ik accepteer de <a
									href="javascript:alert('Wij mogen u aanklagen, u krijgt nooit producten. Durp.');">Gebruikersvoorwaarden</a>.
						</p>

						<p>
							Ga door naar de betaalpagina van uw bank.
						</p>

						<input type="button" value="Annuleren" onclick='location.href="./webshop_winkelwagen.php"'/>
						<input type="submit" name="paid" value="Betalen"/>
					</form>
			<?php
			}
		?>
		</div>
		<footer class="text_padding">
			<?php include 'pages/footer.php'; ?>
		</footer>
	</div>
	</body>
	</html>

<?php
	database_closeConnection($conn);
?>