<?php
	/**
	 * ©2014 Melvin Vermeeren & Jacob Siebelt. All rights reserved.
	 * Created by IntelliJ IDEA.
	 * User: Melvin
	 * Date: 10/03/14
	 * Time: 13:19
	 */
	include('functions_gitignore.php');

	/*
	 * Every page loads functions.php.
	 * So some checks that have to be done always are placed here.
	 */

	/*
	 * Always check if the user is trying to log out.
	 * If so, force the cookies to expire and destroy the session and redirect to the current page.
	 */
	if (array_key_exists('user_logout', $_POST) && $_POST['user_logout'] == true)
	{
		session_destroy();
		setcookie('username', null, 0);
		setcookie('password', null, 0);
		/** @noinspection PhpUndefinedConstantInspection, $_SERVER[HTTP_HOST] /does/ exist. */
		header('Location: ' . $_SERVER['PHP_SELF']);
		exit();
	}

	/*
	 * Always check if the user is trying to log in.
	 */
	if (array_key_exists('username', $_POST) && array_key_exists('password', $_POST))
	{
		if (array_key_exists('remember', $_POST))
		{
			setcookie('username', $_POST['username'], time() + 3600);
			setcookie('password', hash('sha512', $_POST['password'], false), time() + 3600);
		}
		$_SESSION['username'] = $_POST['username'];
		$_SESSION['password'] = hash('sha512', $_POST['password'], false);
	}

	/*
	 * If the user isn't logged in yet but a cookie if available use the cookie to set the session.
	 */
	if ((!array_key_exists('username', $_SESSION) || !array_key_exists('password', $_SESSION)) && array_key_exists('username', $_COOKIE) && array_key_exists('password', $_COOKIE))
	{
		$_SESSION['username'] = $_COOKIE['username'];
		$_SESSION['password'] = $_COOKIE['password'];
	}

	/**
	 * Open a sqlsrv connection.
	 * @param $conn mixed Connection to open.
	 */
	function database_openConnection(&$conn)
	{
		$serverName        = getSQLServiceName();
		$connectionOptions = array
		(
				'Database' => 'LorenzosPizza',
				'UID'      => 'sa',
				'PWD'      => 'root'
		);

		$conn = sqlsrv_connect($serverName, $connectionOptions) or die(sqlsrv_errors());
	}

	/**
	 * Close a sqlsrv connection.
	 * @param $conn mixed Connection to close.
	 */
	function database_closeConnection(&$conn)
	{
		sqlsrv_close($conn);
		$conn = null;
	}

	/**
	 * @param $comboBoxValue mixed Value of the combo box.
	 * @param $input mixed value of the variable.
	 * @return string <code>' selected'</code> if parameters are equal, else <code>''</code>.
	 */
	function isComboBoxSelected($comboBoxValue, $input)
	{
		if ($comboBoxValue == $input) return ' selected';
		else return '';
	}

	function isUserLoggedIn()
	{
		if (!array_key_exists('username', $_SESSION) || !array_key_exists('password', $_SESSION)) return false;

		$query_username = $_SESSION['username'];
		$query_password = $_SESSION['password'];

		database_openConnection($conn);

		$query = sqlsrv_query($conn, "select count(*) from [user] where [username] = '$query_username' and [password] = '$query_password'");
		if ($query === false) exit('<pre>' . print_r(sqlsrv_errors(), true) . '</pre>');
		if (sqlsrv_fetch($query) === false) exit('<pre>' . print_r(sqlsrv_errors(), true) . '</pre>');
		$result = sqlsrv_get_field(($query), 0);

		sqlsrv_free_stmt($query);
		database_closeConnection($conn);
		unset($query_password);
		unset($query);
		unset($conn);

		if ($result > 1) exit("<pre>Error: Duplicate entry in database.<br/>Username: $query_username</pre>");
		unset($query_username);

		if ($result === 1) return true;
		return false;
	}
