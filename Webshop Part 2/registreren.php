<?php
	/**
	 * ©2014 Melvin Vermeeren & Jacob Siebelt. All rights reserved.
	 */
	session_start();
	include('php/functions.php');
	database_openConnection($conn);

	/* Get variables from post or set them to default values. */
	$username = array_key_exists('username', $_POST) ? $_POST['username'] : '';
	$password = array_key_exists('password', $_POST) ? $_POST['password'] : '';
	$repeatPassword = array_key_exists('repeatPassword', $_POST) ? $_POST['repeatPassword'] : '';
	$firstName = array_key_exists('firstName', $_POST) ? $_POST['firstName'] : '';
	$tussenvoegsel = array_key_exists('tussenvoegsel', $_POST) ? $_POST['tussenvoegsel'] : 'null';
	$lastName = array_key_exists('lastName', $_POST) ? $_POST['lastName'] : '';
	$street = array_key_exists('street', $_POST) ? $_POST['street'] : '';
	$houseNumber = array_key_exists('houseNumber', $_POST) ? intval($_POST['houseNumber']) : '0';
	$postalAddress = array_key_exists('postalAddress', $_POST) ? $_POST['postalAddress'] : '';
	$residence = array_key_exists('residence', $_POST) ? $_POST['residence'] : '';
	$email = array_key_exists('email', $_POST) ? $_POST['email'] : '';
	$sex = array_key_exists('sex', $_POST) ? $_POST['sex'] : 'M';
	$phoneNumber = array_key_exists('phoneNumber', $_POST) ? intval($_POST['phoneNumber']) : '';

	/* intval gets rid of any '0's before a number, if phone number length is < 10 keep inserting '0' to fix this problem. */
	if ($phoneNumber == 0) $phoneNumber = null;
	if (strlen($phoneNumber) > 2 && strlen($phoneNumber) < 10)
	{
		$leadingZeros = '0';

		while (strlen($leadingZeros) + strlen($phoneNumber) < 10) $leadingZeros .= '0';

		$phoneNumberReal = $leadingZeros . $phoneNumber;
		unset($leadingZeros);
	}
	else $phoneNumberReal = $phoneNumber;

	/* Validate user's input. */
	$error = '';

	/* Check is username isn't in use. */
	$query = sqlsrv_query($conn, "select count(*) from [user] where [username] = '$username'");
	if ($query === false) exit('<pre>' . print_r(sqlsrv_errors(), true) . '</pre>');
	if (sqlsrv_fetch($query) === false) exit('<pre>' . print_r(sqlsrv_errors(), true) . '</pre>');
	$resultCount = sqlsrv_get_field($query, 0);

	if ($resultCount > 1) exit('<pre>Error: Duplicate entry in table user. Username: ' . $username . '.</pre>');
	else if ($resultCount == 1) $error .= '<br/>Gebruikersnaam is al in gebruik.';

	unset($query);
	unset($resultCount);

	/* Check is all input is valid. */
	if (strlen($username) < 3) $error .= '<br/>Gebruikersnaam moet uit minimaal 3 tekens bestaan.';
	else if (strlen($username) > 15) $error .= '<br/>Gebruikersnaam mag niet meer dan 15 tekens bevatten.';

	if (strlen($password) < 5) $error .= '<br/>Wachtwoord moet uit minimaal 5 tekens bestaan.';
	else if (strlen($password) > 30) $error .= '<br/>Wachtwoord mag niet meer dan 30 tekens bevatten.';

	if ($password !== $repeatPassword) $error .= '<br/>Er zijn twee verschillende wachtwoorden ingevuld.';

	if (strlen($firstName) === 0) $error .= '<br/>Voornaam moet uit minimaal 1 teken bestaan.';
	else if (strlen($firstName) > 128) $error .= '<br/>Voornaam mag niet meer dan 128 tekens bevatten.';

	if (strlen($tussenvoegsel) > 32) $error .= '<br/>Tussenvoegsel mag niet meer dan 32 tekens bevatten.';

	if (strlen($lastName) === 0) $error .= '<br/>Achternaam moet uit minimaal 1 teken bestaan.';
	else if (strlen($lastName) > 128) $error .= '<br/>Achternaam mag niet meer dan 128 tekens bevatten.';

	if (strlen($street) < 3) $error .= '<br/>Straatnaam moet uit minimaal 3 tekens bestaan.';
	else if (strlen($street) > 128) $error .= '<br/>Straatnaam mag niet meer dan 128 tekens bevatten.';

	if (strlen($houseNumber) === 0 || $houseNumber == 0) $error .= '<br/>Huisnummer moet uit minimaal 1 cijfer bestaan.';
	else if (!is_int($houseNumber)) $error .= '<br/>Huisnummer moet uit alleen cijfers bestaan.';

	if (preg_match("#[1-9][0-9][0-9][0-9][A-Z][A-Z]#", $postalAddress) === 0)
		$error .= '<br/>Er moet een geldige postcode ingevoerd worden. Bijvoorbeeld: 1234AB.';

	if (strlen($residence) < 3) $error .= '<br/>Woonplaats moet uit minimaal 1 teken bestaan.';
	else if (strlen($residence) > 128) $error .= '<br/>Woonplaats mag niet meer dan 128 tekens bevatten.';

	if (!filter_var($email, FILTER_VALIDATE_EMAIL) || strlen($email) > 255)
		$error .= '<br/>Er moet een geldig e-mailadres ingevuld worden met een maximale lengte van 255 tekens.';

	if ($sex != 'M' && $sex != 'V') exit('<pre>Error: Invalid sex in registreren.php. Value: ' . $sex . '.</pre>');

	if (!is_int($phoneNumber) || strlen($phoneNumberReal) != 10)
		$error .= '<br/>Er moet een geldig 10-cijferig telefoonnummer gegeven worden.';

	unset($repeatPassword);
?>

	<!DOCTYPE HTML>
	<html lang="nl" dir="ltr">
	<head>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
		<title>Lorenzo&apos;s Pizza - Registreren</title>
		<link rel="shortcut icon" href="images/favicon.ico">
		<link rel="stylesheet" href="css/style.css">
	</head>
	<body>
	<aside id="sidebar_advertisement">
		<?php include 'pages/sidebar.php'; ?>
	</aside>
	<div id="body">
		<header>
			<?php include 'pages/header.php'; ?>
		</header>

		<div class="text_padding">
			<?php
				if (isUserLoggedIn())
				{
					?>
					<div class="text_padding">
						<div class="error_box">
							<h2>U bent ingelogd.</h2>

							<p>
								U kunt niet registreren als u ingelogd bent.<br/>
								<input type="button" value="Naar webshop"
								       onclick="location.href='webshop_producten.php'"/>
								<br/>
							</p>
						</div>
					</div>
				<?php
				}
				else if (array_key_exists('submitPressed', $_POST))
				{
					if ($error !== '')
					{
						?>
						<div class="error_box">
							<h2>U heeft een of meerdere velden foutief ingevuld.</h2>

							<p><?php echo($error); ?></p>
						</div>
					<?php
					}
					else
					{
						$hashedPassword = hash('sha512', $password, false);

						$query = sqlsrv_query($conn,
								"insert into [user]" .
								" ([username], [password], [firstName], [tussenvoegsel], [lastName], [street], [houseNumber], [postalAddress], [residence], [email], [sex], [phoneNumber])" .
								" values" .
								" ('$username', '$hashedPassword', '$firstName', '$tussenvoegsel', '$lastName', '$street', '$houseNumber', '$postalAddress', '$residence', '$email', '$sex', '$phoneNumber')");
						if ($query === false) exit('<pre>' . print_r(sqlsrv_errors(), true) . '</pre>');

						unset($query);
						unset($password);
						unset($hashedPassword);

						?>
						<div class="success_box">
							<h2>U heeft zich succesvol geregistreerd.</h2>

							<p>
								Het systeem heeft u automatisch ingelogd.<br/>
								U kunt nu genieten van onze <i>heerlijke</i> producten.
							</p>
							<input type="button" value="Naar webshop" onclick="location.href='webshop_producten.php'"/>
							<br/>
							<br/>
						</div>
					<?php
					}
				}
				if (!isUserLoggedIn() && $error !== '')
				{
					?>
					<form action="registreren.php" method="post">
						<h1 class="header_center">
							Registreren
						</h1>

						<div>
							<p>
								Vul uw gegevens in:
							</p>
						</div>
						<div id="registreren_invoerlijst_left">
							<h2 style="padding-left: 1%; margin-top:0;">
								Accountgegevens:
							</h2>

							<div class="registreren_accountgegevens">
								Gebruikersnaam*
								<br/>
								<input type="text" name="username" value="<?php echo($username); ?>" size="49"/>
							</div>
							<div class="registreren_accountgegevens">
								Wachtwoord*
								<br/>
								<input type="password" name="password" size="49"/>
							</div>
							<div class="registreren_accountgegevens">
								HerhaalWachtwoord*
								<br/>
								<input type="password" name="repeatPassword" size="49"/>
							</div>
						</div>
						<div id="registreren_invoerlijst_right">
							<h2 style="padding-left: 1%; margin-top: 0;">
								Factuuradres:
							</h2>

							<div id="registreren_aanhef">
								Aanhef*
								<br/>
								<select name="sex">
									<option value="M"<?php echo(isComboBoxSelected('M', $sex)); ?>>
										Dhr.
									</option>
									<option value="V"<?php echo(isComboBoxSelected('V', $sex)); ?>>
										Mev.
									</option>
								</select>
							</div>
							<div class="registreren_voornaamtussenv">
								Voornaam*
								<br/>
								<input type="text" name="firstName" value="<?php echo($firstName); ?>"/>
							</div>
							<div class="registreren_voornaamtussenv">
								Tussenvoegsels
								<br/>
								<input type="text" name="tussenvoegsel"
								       value="<?php echo(($tussenvoegsel === 'null') ? '' : $tussenvoegsel); ?>"
								       size="5"/>
							</div>
							<div id="registreren_achternaam">
								Achternaam*
								<br/>
								<input type="text" name="lastName" value="<?php echo($lastName); ?>" size="28"/>
							</div>
							<div id="registreren_email">
								E-mailadres*
								<br/>
								<input type="text" name="email" value="<?php echo($email); ?>" size="110"/>
							</div>
							<div class="registreren_strtnaampostcode">
								Straatnaam*
								<br/>
								<input type="text" name="street" value="<?php echo($street); ?>" size="50"/>
							</div>
							<div class="registreren_huisnummerplaatsnaamtelefoon">
								Huisnummer*
								<br/>
								<input type="text" name="houseNumber"
								       value="<?php echo(($houseNumber == 0) ? '' : $houseNumber); ?>" size="50"/>
							</div>
							<div class="registreren_strtnaampostcode">
								Postcode*
								<br/>
								<input type="text" name="postalAddress" value="<?php echo($postalAddress); ?>"
								       size="50"/>
							</div>
							<div class="registreren_huisnummerplaatsnaamtelefoon">
								Plaatsnaam*
								<br/>
								<input type="text" name="residence" value="<?php echo($residence); ?>" size="50"/>
							</div>
							<div class="registreren_huisnummerplaatsnaamtelefoon">
								Telefoon*
								<br/>
								<input type="text" name="phoneNumber"
								       value="<?php echo($phoneNumberReal); ?>"
								       size="50"/>
							</div>
						</div>
						<div id="registreren_submit_button">
							<input type="submit" name="submitPressed" value="Registreren">
						</div>
					</form>
				<?php
				}
			?>
		</div>
		<footer class="text_padding">
			<?php include 'pages/footer.php'; ?>
		</footer>
	</div>
	</body>
	</html>

<?php
	database_closeConnection($conn);
?>