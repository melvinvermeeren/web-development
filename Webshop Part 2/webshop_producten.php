<?php
	/**
	 * ©2014 Melvin Vermeeren & Jacob Siebelt. All rights reserved.
	 */
	session_start();
	include('php/functions.php');
	database_openConnection($conn);

	/* Get variables from post, or set them to default values. */
	$category = ((array_key_exists('category', $_POST)) ? ($_POST['category']) : ('All'));
	$subcategory = ((array_key_exists('subcategory', $_POST)) ? ($_POST['subcategory']) : ('All'));
	$productCountPage = ((array_key_exists('productCountPage', $_POST)) ? ($_POST['productCountPage']) : (10));
	$search = html_entity_decode((array_key_exists('search', $_POST)) ? ($_POST['search']) : (''));
	$page = ((array_key_exists('page', $_POST)) ? ($_POST['page']) : (1));

	/* Configure SQL where-clause. */
	$filter = '';
	if ($category !== 'All' || $subcategory !== 'All' || $search !== '')
	{
		$filter = ' where ';

		if ($category !== 'All') $filter .= '[category] = \'' . $category . '\'';
		if ($subcategory !== 'All')
		{
			if ($category !== 'All') $filter .= ' and ';
			$filter .= '[subcategory] = \'' . $subcategory . '\'';
		}
		if ($search !== '')
		{
			$searchArray = explode(' ', $search);
			if ($subcategory !== 'All' || $category !== 'All') $filter .= ' and ';

			$firstLoop = true;
			foreach ($searchArray as $searchElement)
			{
				if (!$firstLoop) $filter .= ' or ';
				$filter .= '[name] like \'%' . $searchElement . '%\'';
				if ($firstLoop) $firstLoop = false;
			}

			unset($searchArray);
			unset($searchElement);
		}
	}

	/* Get total number of products. Query it, fetch the first row, and get the first (and only) field value from that row. */
	$productCountTotalQuery = sqlsrv_query($conn, 'select count(*) from [product]' . $filter);
	if (sqlsrv_fetch($productCountTotalQuery) === false) exit('<pre>' . print_r(sqlsrv_errors(), true) . '</pre>');
	$productCountTotal = sqlsrv_get_field(($productCountTotalQuery), 0);

	/* Calculate number of pages. */
	$pagesCount = ceil($productCountTotal / $productCountPage);
	if ($page > $pagesCount) $page = 1;

	/* Configure final SQL where-clause including page numbering. */
	if ($page > 1)
	{
		$isOnlyWhereClause = true;

		if (strlen($filter) > 0)
		{
			$isOnlyWhereClause = false;
			$filterPage        = $filter;
			$filterPage .= ' and ';
		}
		else $filterPage = ' where ';

		$filterPage .= '[number] not in (select top(' . (($page - 1) * $productCountPage) . ') [number] from [product]' . ($filter) . ' order by [name])';

		unset($isOnlyWhereClause); // Destroy this temporarily variable.
	}
	else $filterPage = $filter;

	/* Main query for the products. */
	$query = sqlsrv_query($conn, 'select top(' . $productCountPage . ') [number], [name], [price], [discountedPrice], [stock], [image] from [product]' . $filterPage . ' order by [name]');
	if ($query === false) exit('<pre>' . print_r(sqlsrv_errors(), true) . '</pre>');
?>

<!DOCTYPE HTML>
<html lang="nl" dir="ltr">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
	<title>Lorenzo&apos;s Pizza - Webshop producten</title>
	<link rel="shortcut icon" href="images/favicon.ico">
	<link rel="stylesheet" href="css/style.css">
</head>
<body>
<aside id="sidebar_advertisement">
	<?php include 'pages/sidebar.php'; ?>
</aside>
<div id="body">
	<header>
		<?php include 'pages/header.php'; ?>
	</header>

	<div class="text_padding">
		<form action="webshop_producten.php" method="post">
			<div id="webshop_producten_search">
				<input name="search" type="text" value="<?php echo($search); ?>" placeholder="Zoekterm..."/>
				<input type="submit" value="Zoek"/>
			</div>
			<p style="margin: 0;">
				<label>
					<select name="category" onchange="this.form.submit();">
						<option value="All"
								<?php echo(isComboBoxSelected('All', $category)); ?>>Alle Gerechten
						</option>
						<?php
							$queryCategories = sqlsrv_query($conn, 'select [category], [translation] from [category]');
							if ($queryCategories === false) exit('<pre>' . print_r(sqlsrv_errors(), true) . '</pre>');

							while ($rowCategories = sqlsrv_fetch_array($queryCategories))
							{
								echo('<option value="' . $rowCategories['category'] . '"');
								echo(isComboBoxSelected($rowCategories['category'], $category));
								echo('>' . $rowCategories['translation'] . '</option>');
							}

							sqlsrv_free_stmt($queryCategories);
							unset($queryCategories);
							unset($rowCategories);
						?>
					</select>
				</label>
				<label>
					<select name="subcategory" onchange="this.form.submit();">
						<option value="All"
								<?php echo(isComboBoxSelected('All', $subcategory)); ?>>Alle
						</option>
						<?php
							$querySubcategories = sqlsrv_query($conn, 'select [subcategory], [translation] from [subcategory]');
							if ($querySubcategories === false) exit('<pre>' . print_r(sqlsrv_errors(), true) . '</pre>');

							while ($rowSubcategories = sqlsrv_fetch_array($querySubcategories))
							{
								echo('<option value="' . $rowSubcategories['subcategory'] . '"');
								echo(isComboBoxSelected($rowSubcategories['subcategory'], $subcategory));
								echo('>' . $rowSubcategories['translation'] . '</option>');
							}

							sqlsrv_free_stmt($querySubcategories);
							unset($querySubcategories);
							unset($rowSubcategories);
						?>
					</select>
				</label>
				<?php echo($productCountTotal . ' ' . (($productCountTotal === 1) ? ('product') : ('producten')) . '.'); ?>
				| Toon
				<label>
					<select name="productCountPage" onchange="this.form.submit();">
						<option value="3"
								<?php echo(isComboBoxSelected(3, $productCountPage)); ?>>3
						</option>
						<option value="5"
								<?php echo(isComboBoxSelected(5, $productCountPage)); ?>>5
						</option>
						<option value="10"
								<?php echo(isComboBoxSelected(10, $productCountPage)); ?>>10
						</option>
						<option value="20"
								<?php echo(isComboBoxSelected(20, $productCountPage)); ?>>20
						</option>
						<option value="50"
								<?php echo(isComboBoxSelected(50, $productCountPage)); ?>>50
						</option>
					</select>
				</label>
				per pagina. | Pagina
				<label>
					<select name="page" onchange="this.form.submit();">
						<?php
							for ($i = 1; $i <= $pagesCount; $i++)
							{
								?>
								<option value="<?php echo($i); ?>"
										<?php echo(isComboBoxSelected($i, $page)); ?>><?php echo($i); ?></option>
							<?php
							}
						?>
					</select>
				</label>
				van de <?php echo($pagesCount) ?>.
		</form>
		<br>

		<div id="webshop_producten_products">
			<?php
				while ($row = sqlsrv_fetch_array($query))
				{
					?>
					<div class="webshop_producten_product">
						<a href="./productpagina.php?product=<?php echo($row['number']); ?>"><img
									class="webshop_producten_product_image"
									src="<?php echo($row['image']); ?>"
									alt="<?php echo($row['name']); ?>"/></a>

						<p><?php echo($row['name']); ?></p>

						<?php
							if ($row['stock'] == 0) echo('<span class="product_sold_out">Uitverkocht</span>');
							else if ($row['discountedPrice'] != 0)
							{
								echo('<span class="product_discount">&euro;' . str_replace('.', ',', $row['price']) . '</span>');
								echo(' &euro;' . str_replace('.', ',', $row['discountedPrice']));
							}
							else echo('&euro;' . str_replace('.', ',', $row['price']));

							if ($row['stock'] == 0) echo('<br/><br/>');
							else
							{
								?>

								<form action="webshop_winkelwagen.php" method="post">
									<input type="hidden" name="addproduct"
									       value="<?php echo($row['number']); ?>"/>
									<input type="hidden" name="quantity"
									       value="1"/>
									<input type="submit"
									       value="In winkelwagen"/>
								</form>
							<?php
							}
						?>
						<br/>
					</div>
				<?php
				}
			?>
		</div>
	</div>

	<footer class="text_padding">
		<?php include 'pages/footer.php'; ?>
	</footer>
</div>
</body>
</html>

<?php
	sqlsrv_free_stmt($query);
	sqlsrv_free_stmt($productCountTotalQuery);
	database_closeConnection($conn);
?>
