<div id="header_user">
	<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
		<p>
			<?php
				$isUserLoggedIn = isUserLoggedIn();
				echo(($isUserLoggedIn ? $_SESSION['username'] : 'Inloggen'));
				echo('<br/>');
			?>
			<span class="header_user_hidden">
			<?php include(($isUserLoggedIn ? 'user_loggedin.php' : 'user_notloggedin.php')); ?>
		</span>
		</p>
	</form>
</div>
