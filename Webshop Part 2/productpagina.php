<?php
	/**
	 * ©2014 Melvin Vermeeren & Jacob Siebelt. All rights reserved.
	 */
	session_start();
	include('php/functions.php');
	database_openConnection($conn);

	/* Get variables from get, or set them to default values. */
	$number = ((array_key_exists('product', $_GET)) ? ($_GET['product']) : (1));

	$query = sqlsrv_query(
			$conn,
			"select [number], [name], [description], [price], [discountedPrice], [stock], [image] from [product] where [number] = $number");
	if ($query === false) exit('<pre>' . print_r(sqlsrv_errors(), true) . '</pre>');

	$row = sqlsrv_fetch_array($query);
	$rowNumber = $row['number'];

	/* Configure SQL where-clause. */
	$filter = '';
	$rowNameSplit = explode(' ', $row['name']);
	$isFirstWord = true;
	foreach ($rowNameSplit as $word)
	{
		if (strlen($word) > 3)
		{
			if (!$isFirstWord) $filter .= ' or ';

			$filter .= '[name] like \'%' . $word . '%\'';

			if ($isFirstWord) $isFirstWord = false;
		}
	}
	unset($rowNameSplit);
	unset($isFirstWord);

	$queryRelatedProducts = sqlsrv_query(
			$conn,
			"select top(3) [number], [name], [price], [discountedPrice], [stock], [image] from [product] where [number] <> '$rowNumber' and ($filter) order by newid()");
	if ($queryRelatedProducts === false) exit('<pre>' . print_r(sqlsrv_errors(), true) . '</pre>');
?>

<!DOCTYPE HTML>
<html lang="nl" dir="ltr">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
	<title>Lorenzo&apos;s Pizza - <?php echo($row['name']); ?></title>
	<link rel="shortcut icon" href="images/favicon.ico">
	<link rel="stylesheet" href="css/style.css">
</head>
<body>
<aside id="sidebar_advertisement">
	<?php include 'pages/sidebar.php'; ?>
</aside>
<div id="body">
	<header>
		<?php include 'pages/header.php'; ?>
	</header>
	<div class="text_padding">
		<div id="product_highlight">
			<h1 id="product_highlight_title">
				<?php echo($row['name']); ?>
			</h1>
			<img id="product_big_image" src="<?php echo($row['image']); ?>" alt="<?php echo($row['name']); ?>"/>
			<?php echo($row['description']); ?>
			<br/>

			<div id="product_highlight_options">
				<form action="webshop_winkelwagen.php" method="post">
					<input type="hidden" name="addproduct" value="<?php echo($row['number']); ?>"/>
					<span class="product_highlight_span">
						Op voorraad: <?php echo($row['stock']); ?>
					</span>
					<span class="product_highlight_span">
						<?php
							if ($row['discountedPrice'] != 0)
							{
								echo('<span class="product_discount">&euro;' . str_replace('.', ',', $row['price']) . '</span>');
								echo(' &euro;' . str_replace('.', ',', $row['discountedPrice']));
							}
							else echo('&euro;' . str_replace('.', ',', $row['price']));

							if ($row['stock'] == 0)
							{
								echo('<span class="product_sold_out"><br/>Uitverkocht</span>');
							}
							else
							{
						?>
					</span>
					<span class="product_highlight_span">
						Aantal:
						<label>
							<select name="quantity">
								<?php
									for ($i = 1; $i <= $row['stock']; $i++)
									{
										?>
										<option value="<?php echo($i); ?>"><?php echo($i); ?></option>
									<?php
									}
								?>
							</select>
						</label>
					</span>
					<br/>
					<span class="product_highlight_span">
						<input type="submit" value="In winkelwagen"/>
					</span>
					<?php
						}
					?>
				</form>
			</div>
		</div>
		<div id="product_moreproducts">
			<?php
				while ($rowRelated = sqlsrv_fetch_array($queryRelatedProducts))
				{
					?>
					<div class="webshop_producten_product">
						<a href="./productpagina.php?product=<?php echo($rowRelated['number']); ?>"><img
									class="webshop_producten_product_image"
									src="<?php echo($rowRelated['image']); ?>"
									alt="<?php echo($rowRelated['name']); ?>"/></a>

						<p><?php echo($rowRelated['name']); ?></p>

						<?php
							if ($rowRelated['stock'] == 0) echo('<span class="product_sold_out">Uitverkocht</span>');
							else if ($rowRelated['discountedPrice'] != 0)
							{
								echo('<span class="product_discount">&euro;' . str_replace('.', ',', $rowRelated['price']) . '</span>');
								echo(' &euro;' . str_replace('.', ',', $rowRelated['discountedPrice']));
							}
							else echo('&euro;' . str_replace('.', ',', $rowRelated['price']));

							if ($rowRelated['stock'] == 0) echo('<br/><br/>');
							else
							{
								?>

								<form action="webshop_winkelwagen.php" method="post">
									<input type="hidden" name="addproduct"
									       value="<?php echo($rowRelated['number']); ?>"/>
									<input type="hidden" name="quantity"
									       value="1"/>
									<input type="submit"
									       value="In winkelwagen"/>
								</form>
							<?php
							}
						?>
						<br/>
					</div>
				<?php
				}
			?>
		</div>
	</div>
	<footer class="text_padding">
		<?php include 'pages/footer.php'; ?>
	</footer>
</div>
</body>
</html>

<?php
	sqlsrv_free_stmt($query);
	database_closeConnection($conn);
?>
