<?php
	/**
	 * ©2014 Melvin Vermeeren & Jacob Siebelt. All rights reserved.
	 */
	session_start();
	include('php/functions.php');
	database_openConnection($conn);

	/* Get variables from post, or set them to default values. */
	$addproduct = ((array_key_exists('addproduct', $_POST)) ? ($_POST['addproduct']) : (''));
	$quantity = ((array_key_exists('quantity', $_POST)) ? ($_POST['quantity']) : (''));
	$removeProduct = ((array_key_exists('removeProduct', $_POST)) ? ($_POST['removeProduct']) : (''));
	$username = ((array_key_exists('username', $_SESSION)) ? ($_SESSION['username']) : (''));
	$setProduct = ((array_key_exists('setProduct', $_POST)) ? ($_POST['setProduct']) : (''));
	$setQuantity = ((array_key_exists('setQuantity', $_POST)) ? ($_POST['setQuantity']) : (''));
	$stockError = ((array_key_exists('stockError', $_POST)) ? ($_POST['stockError']) : (''));
	$stockErrorProductNumber = ((array_key_exists('stockErrorProductNumber', $_POST)) ? ($_POST['stockErrorProductNumber']) : (''));
	$stockErrorGoToWinkelwagen = false;

	/* Configure SQL where-clause.*/

	if (isUserLoggedIn())
	{
		if ($removeProduct !== '')
		{
			$removeProductQuery = sqlsrv_query(
					$conn,
					"delete from [cart] where [username] = '$username' and [number] = '$removeProduct'");
			if ($removeProductQuery === false) exit('<pre>' . print_r(sqlsrv_errors(), true) . '</pre>');
		}

		if ($addproduct !== '' && $quantity !== '' && $_SESSION['username'] !== '')
		{
			$numberAlreadyInCartQuery = sqlsrv_query($conn, "select count(*) from [cart] where [username] = '$username' and [number] = $addproduct");
			if (sqlsrv_fetch($numberAlreadyInCartQuery) === false) exit('<pre>' . print_r(sqlsrv_errors(), true) . '</pre>');
			$numberAlreadyInCartCount = sqlsrv_get_field(($numberAlreadyInCartQuery), 0);

			if ($numberAlreadyInCartCount > 1) exit("<pre>Error: Duplicate entry in database.</pre>");

			if ($numberAlreadyInCartCount == 1)
			{
				$updateCart = sqlsrv_query($conn, "update [cart] set [quantity] += '$quantity' where [username] = '$username' and [number] = '$addproduct'");
			}
			else
			{
				$query = sqlsrv_query(
						$conn,
						"insert into [cart] ([username], [number], [quantity]) values ('$username', '$addproduct', '$quantity')");
				if ($query === false) exit('<pre>' . print_r(sqlsrv_errors(), true) . '</pre>');
				sqlsrv_free_stmt($query);
				unset($query);
			}

			sqlsrv_free_stmt($numberAlreadyInCartQuery);
			unset($numberAlreadyInCartQuery);
			unset($numberAlreadyInCartCount);
		}

		/* Update quantity if product is modified. */
		if ($setProduct !== '' && $setQuantity !== '')
		{
			$query = sqlsrv_query(
					$conn,
					"update [cart] set [quantity] = '$setQuantity' where [username] = '$username' and [number] = '$setProduct'");
			if ($query === false) exit('<pre>' . print_r(sqlsrv_errors(), true) . '</pre>');
			sqlsrv_free_stmt($query);
			unset($query);
		}

		/* Check for invalid entries, further processed by the block below. */
		if ($stockError == '' && $stockErrorProductNumber == '')
		{
			$quantityAndNumberQuery = sqlsrv_query($conn, "select [number], [quantity] from [cart] where [username] = '$username'");
			if ($quantityAndNumberQuery === false) exit('<pre>' . print_r(sqlsrv_errors(), true) . '</pre>');

			$productQuery = sqlsrv_query(
					$conn,
					"select [number], [stock] from [product] where [number] in
			(select [number] from [cart] where [username] = '$username')",
					array(), array("Scrollable" => SQLSRV_CURSOR_KEYSET));
			if ($productQuery === false) exit('<pre>' . print_r(sqlsrv_errors(), true) . '</pre>');

			$quantityAndNumberQueryClone = sqlsrv_query($conn, "select [number], [quantity] from [cart] where [username] = '$username'");
			if ($quantityAndNumberQueryClone === false) exit('<pre>' . print_r(sqlsrv_errors(), true) . '</pre>');

			while ($row = sqlsrv_fetch_array($quantityAndNumberQuery))
			{
				while ($rowClone = sqlsrv_fetch_array($quantityAndNumberQueryClone))
				{
					$rowTwo = sqlsrv_fetch_array($productQuery);
					if ($rowClone['quantity'] > $rowTwo['stock'])
					{
						$stockErrorProductNumber = $rowTwo['number'];
						$stockError              = true;
						break;
					}
				}
				if ($stockErrorGoToWinkelwagen) break;
			}
		}

		/* Check for errors in stock, this happens when stock has changed since adding to cart when on page webshop_afrekenen.php or in the block above. */
		if ($stockError != '' && $stockErrorProductNumber != '')
		{
			/* If product is sold out don't do anything, else update to new maximum amount. */
			$queryStock = sqlsrv_query($conn, "select [stock] from [product] where [number] = '$stockErrorProductNumber'");
			if ($queryStock === false) exit('<pre>' . print_r(sqlsrv_errors(), true) . '</pre>');
			if (sqlsrv_fetch($queryStock) === false) exit('<pre>' . print_r(sqlsrv_errors(), true) . '</pre>');
			$newStock = sqlsrv_get_field(($queryStock), 0);

			if ($newStock > 0)
			{
				$query = sqlsrv_query(
						$conn,
						"update [cart] set [quantity] = '$newStock' where [username] = '$username' and [number] = '$stockErrorProductNumber'");
				if ($query === false) exit('<pre>' . print_r(sqlsrv_errors(), true) . '</pre>');
				sqlsrv_free_stmt($query);
				unset($query);
			}

			sqlsrv_free_stmt($queryStock);
			unset($queryStock);
			unset($newStock);
		}

		/* Main query. */
		$queryCart = sqlsrv_query($conn, "select [quantity] from [cart] where [username] = '$username'");
		if ($queryCart === false) exit('<pre>' . print_r(sqlsrv_errors(), true) . '</pre>');

		$query = sqlsrv_query(
				$conn,
				"select [number], [name], [price], [discountedPrice], [stock], [image] from [product] where [number] in
			(select [number] from [cart] where [username] = '$username')",
				array(), array("Scrollable" => SQLSRV_CURSOR_KEYSET));
		if ($query === false) exit('<pre>' . print_r(sqlsrv_errors(), true) . '</pre>');

		$cartProductCount = sqlsrv_num_rows($query);
		if ($cartProductCount === false) exit('<pre>' . print_r(sqlsrv_errors(), true) . '</pre>');
	}
?>

	<!DOCTYPE HTML>
	<html lang="nl" dir="ltr">
	<head>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
		<title>Lorenzo&apos;s Pizza - Webshop winkelwagen</title>
		<link rel="shortcut icon" href="images/favicon.ico">
		<link rel="stylesheet" href="css/style.css">
	</head>
	<body>
	<aside id="sidebar_advertisement">
		<?php include 'pages/sidebar.php'; ?>
	</aside>
	<div id="body">
		<header>
			<?php include 'pages/header.php'; ?>
		</header>
		<?php
			if (!isUserLoggedIn())
			{
				?>
				<div class="text_padding">
					<div class="info_box">
						<h2>U bent niet ingelogd.</h2>

						<p>
							U moet ingelogd zijn om bij uw winkelwagen te kunnen.<br/>
							Log rechtsboven op de pagina in of registreer.<br/>
							<input type="button" value="Registreren" onclick="location.href='registreren.php'"/>
							<br/>
						</p>
					</div>
				</div>
			<?php
			}
			else if ($cartProductCount === 0)
			{
				?>
				<div class="text_padding">
					<div class="info_box">
						<h2>Uw winkelwagen is leeg.</h2>

						<p>
							Ga naar de webshop om producten in de winkelwagen te doen.<br/>
							<input type="button" value="Naar webshop" onclick="location.href='webshop_producten.php'"/>
							<br/>
						</p>
					</div>
				</div>
			<?php
			}
			else
			{
				?>
				<div class="text_padding">
					<?php
						if ($stockError != '')
						{
							?>
							<div class="error_box">
								<h2>Één of meerdere producten zijn uitverkocht of niet in gewenste hoeveeleid
								    aanwezig.</h2>

								<p>
									Sinds dat u een product in het winkelmandje heeft toegevoegd heeft iemand anders
									hetzelfde
									product gekocht waardoor het of uitverkocht is of niet in de gewenste hoeveelheid
									aanwezig is.
									<br/>
									<br/>
									Het betreffende product:
									<?php
										$queryStockErrorName = sqlsrv_query($conn, "select [name] from [product] where [number] =  '$stockErrorProductNumber'");
										if ($queryStockErrorName === false) exit('<pre>' . print_r(sqlsrv_errors(), true) . '</pre>');
										if (sqlsrv_fetch($queryStockErrorName) === false) exit('<pre>' . print_r(sqlsrv_errors(), true) . '</pre>');
										echo(sqlsrv_get_field(($queryStockErrorName), 0) . '.');
									?>
								</p>
							</div>
						<?php
						}
					?>
					<input type="button" value="Verder winkelen" onclick='location.href="./webshop_producten.php"'/>

					<h1 id="webshop_winkelwagen_header">Winkelwagen</h1>
					<table id="webshop_winkelwagen_table_main" class="webshop_winkelwagen_table">
						<tr>
							<th>Productfoto</th>
							<th>Productnaam</th>
							<th>Prijs</th>
							<th>Aantal</th>
							<th>Subtotaal</th>
							<th>Verwijderen</th>
						</tr>
						<?php
							$totalPrice = 0;
							$totalPriceDiscounted = 0;

							while ($row = sqlsrv_fetch_array($query))
							{
								$rowCart = sqlsrv_fetch_array($queryCart);
								?>
								<tr>
									<td class="webshop_winkelwagen_table_images">
										<a href="./productpagina.php?product=<?php echo($row['number']) ?>">
											<img class="webshop_winkelwagen_table_images_image"
											     src="<?php echo($row['image']); ?>" alt="Spaghetti met krab"/>
										</a>
									</td>
									<td class="webshop_winkelwagen_table_text">
										<?php echo($row['name']) ?>
									</td>
									<td class="webshop_winkelwagen_table_text">
										<?php
											if ($row['discountedPrice'] != 0)
											{
												echo('<span class="product_discount">&euro;' . str_replace('.', ',', $row['price']) . '</span><br/>');
												echo('&euro;' . str_replace('.', ',', $row['discountedPrice']));
											}
											else echo('&euro;' . str_replace('.', ',', $row['price']));
										?>
									</td>
									<td class="webshop_winkelwagen_table_text">
										<form action="webshop_winkelwagen.php" method="post">
											<input type="hidden" name="changeQuantity">
											<input type="hidden" name="setProduct"
											       value="<?php echo($row['number']); ?>"/>
											<?php
												if ($row['stock'] == 0)
												{
													echo('<span class="product_sold_out">Uitverkocht</span>');
												}
												else
												{
													?>
													<label>
														<select name="setQuantity" onchange="this.form.submit();">
															<?php
																for ($i = 1; $i <= $row['stock']; $i++)
																{
																	echo('<option value="' . $i . '"' . isComboBoxSelected($i, $rowCart['quantity']) . '>' . $i . '</option>');
																}
															?>
														</select>
													</label>
												<?php
												}
											?>
										</form>
									</td>
									<td class="webshop_winkelwagen_table_text">
										<?php
											if ($row['stock'] == 0) echo('-');
											else if ($row['discountedPrice'] != 0)
											{
												$totalPrice += ($row['price'] * $rowCart['quantity']);
												$totalPriceDiscounted += ($row['discountedPrice'] * $rowCart['quantity']);
												echo('<span class="product_discount">&euro;');
												echo(str_replace('.', ',', (number_format(($row['price'] * $rowCart['quantity']), 2, '.', ','))));
												echo('</span><br/>');
												echo('&euro;' . str_replace('.', ',', (number_format(($row['discountedPrice'] * $rowCart['quantity']), 2, '.', ','))));
											}
											else
											{
												$totalPrice += ($row['price'] * $rowCart['quantity']);
												$totalPriceDiscounted += ($row['price'] * $rowCart['quantity']);
												echo('&euro;' . str_replace('.', ',', (number_format(($row['price'] * $rowCart['quantity']), 2, '.', ','))));
											}
										?>
									</td>
									<td class="webshop_winkelwagen_table_text">
										<form action="webshop_winkelwagen.php" method="post">
											<input type="hidden" name="removeProduct"
											       value="<?php echo($row['number']) ?>"/>
											<input type="submit" value="" class="webshop_winkelwagen_table_remove"/>
										</form>
									</td>
								</tr>
							<?php
							}
						?>
					</table>
					<div style="text-align: right;">
						<table id="webshop_winkelwagen_table_total" class="webshop_winkelwagen_table">
							<tr>
								<th>Eindtotaal</th>
								<td class="webshop_winkelwagen_table_text">
									<?php
										if ($totalPrice !== $totalPriceDiscounted)
										{
											echo('<span class="product_discount">');
											echo('&euro;' . str_replace('.', ',', (number_format($totalPrice, 2, '.', ','))));
											echo('</span><br/>');
										}
										echo('&euro;' . str_replace('.', ',', (number_format($totalPriceDiscounted, 2, '.', ','))));
									?>
								</td>
							</tr>
						</table>
						<br/>

						<form action="webshop_afrekenen.php" method="post">
							<input type="hidden" value="<?php echo($username); ?>"/>
							<input type="submit" value="Afrekenen"/>
						</form>
					</div>
				</div>
			<?php
			}
		?>
		<footer class="text_padding">
			<?php include 'pages/footer.php'; ?>
		</footer>
	</div>
	</body>
	</html>

<?php
	database_closeConnection($conn);
?>