<?php
	/**
	 * ©2014 Melvin Vermeeren & Jacob Siebelt. All rights reserved.
	 */
	session_start();
	include('php/functions.php');
	database_openConnection($conn);

	/* Main query. */
	$query = sqlsrv_query($conn,
			"select [number], [name], [price], [discountedPrice], [stock], [image] from [product] where [discountedPrice] <> 0",
			array(),
			array("Scrollable" => SQLSRV_CURSOR_KEYSET));
	if ($query === false) exit('<pre>' . print_r(sqlsrv_errors(), true) . '</pre>');

	$discountedProductCount = sqlsrv_num_rows($query);
	if ($discountedProductCount === false) exit('<pre>' . print_r(sqlsrv_errors(), true) . '</pre>');
?>

	<!DOCTYPE HTML>
	<html lang="nl" dir="ltr">
	<head>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
		<title>Lorenzo&apos;s Pizza - Acties</title>
		<link rel="shortcut icon" href="images/favicon.ico">
		<link rel="stylesheet" href="css/style.css">
	</head>
	<body>
	<aside id="sidebar_advertisement">
		<?php include 'pages/sidebar.php'; ?>
	</aside>
	<div id="body">
		<header>
			<?php include 'pages/header.php'; ?>
		</header>

		<div class="text_padding">
			<div>
				<h1 class="header_center">Acties</h1>

				<p>
					<?php
						if ($discountedProductCount == 0) echo('Er zijn op dit moment geen producten in de aanbieding.<br/>Kijk later nog eens.');
						else
						{
							if ($discountedProductCount == 1) echo('Dit <em>ambachtelijke</em> gerecht is in de aanbieding.');
							else echo('Deze <em>ambachtelijke</em> gerechten zijn in de aanbieding.');
							?>
							Bereid door echte italiaanse vakmensen bereid in hand-gemaakte steenovens en met ander fascineerd kookgerei.
							Koop vandaag bij <i>Lorenzo&apos;s Pizza</i>!
						<?php
						}
					?>
				</p>

				<div id="webshop_producten_products">
					<?php
						while ($row = sqlsrv_fetch_array($query))
						{
							?>
							<div class="webshop_producten_product">
								<a href="./productpagina.php?product=<?php echo($row['number']); ?>"><img
											class="webshop_producten_product_image"
											src="<?php echo($row['image']); ?>"
											alt="<?php echo($row['name']); ?>"/></a>

								<p><?php echo($row['name']); ?></p>

								<?php
									if ($row['stock'] == 0) echo('<span class="product_sold_out">Uitverkocht</span>');
									else if ($row['discountedPrice'] != 0)
									{
										echo('<span class="product_discount">&euro;' . str_replace('.', ',', $row['price']) . '</span>');
										echo(' &euro;' . str_replace('.', ',', $row['discountedPrice']));
									}
									else echo('&euro;' . str_replace('.', ',', $row['price']));

									if ($row['stock'] == 0) echo('<br/><br/>');
									else
									{
										?>

										<form action="webshop_winkelwagen.php" method="post">
											<input type="hidden" name="addproduct"
											       value="<?php echo($row['number']); ?>"/>
											<input type="hidden" name="quantity"
											       value="1"/>
											<input type="submit"
											       value="In winkelwagen"/>
										</form>
									<?php
									}
								?>
								<br/>
							</div>
						<?php
						}
					?>
				</div>
			</div>
		</div>

		<footer class="text_padding">
			<?php include 'pages/footer.php'; ?>
		</footer>
	</div>
	</body>
	</html>

<?php
	sqlsrv_free_stmt($query);
	database_closeConnection($conn);
?>