/**
 * ©2014 Melvin Vermeeren & Jacob Siebelt. All rights reserved.
 */


------------------------
-- Preparation steps. --
------------------------
use [master]
go

if exists(select
	          [name]
          from [sys].[databases]
          where [name] = 'LorenzosPizza')
	begin
		alter database [LorenzosPizza] set single_user
		with rollback immediate
		drop database [LorenzosPizza]
	end
go

use [master]
go

create database [LorenzosPizza]
go


--------------------
-- Create tables. --
--------------------
use [LorenzosPizza]
go


create table [category]
(
	[category] varchar(30) not null primary key check (len([category]) >= 3),
	[translation] varchar(30) not null check (len([translation]) >= 3)
);
go

create table [subcategory]
(
	[subcategory] varchar(30) not null primary key check (len([subcategory]) >= 3),
	[translation] varchar(30) not null check (len([translation]) >= 3)
);
go

create table [user]
(
	[username]      varchar(15)    not null primary key check (len([username]) >= 3),
	[password]      char(128)      not null check (len([password]) = 128), -- SHA512
	[firstName]     varchar(128)   not null check (len([firstName]) > 0),
	[tussenvoegsel] varchar(32)    null,
	[lastName]      varchar(128)   not null check (len([lastName]) > 0),
	[street]        varchar(128)   not null check (len([street]) >= 3),
	[houseNumber]   integer        not null,
	[postalAddress] char(6)        not null check ([postalAddress] like '[1-9][0-9][0-9][0-9][A-Z][A-Z]'),
	[residence]     varchar(128)   not null check (len([residence]) >= 3),
	[email]         varchar(255)   not null,
	[sex]           char(1)        not null check ([sex] in ('M', 'V')),
	[phoneNumber]   numeric(10, 0) not null check (len([phoneNumber]) > 0 or len([phoneNumber]) <= 10),
-- numeric values can't start with 0, so 0123 => 123. Allow phone numbers starting with 0.

	check -- Check for valid email, source: http://vyaskn.tripod.com/handling_email_addresses_in_sql_server.htm.
	(
		charindex(' ', ltrim(rtrim([email]))) = 0 -- No embedded spaces.
		and left(ltrim([email]), 1) <> '@'  -- '@' can't be the first character of an email address.
		and right(rtrim([email]), 1) <> '.' -- '.' can't be the last character of an email address.
		and charindex('.', [email], charindex('@', [email])) - charindex('@', [email]) > 1 -- There must be a '.' after '@'.
		and len(ltrim(rtrim([email]))) - len(replace(ltrim(rtrim([email])), '@', '')) = 1 -- Only one '@' sign is allowed.
		and charindex('.', reverse(ltrim(rtrim([email])))) >= 3
		-- Domain name should end with at least 2 character extension.
		and (charindex('.@', [email]) = 0 and charindex('..', [email]) = 0) -- can't have patterns like '.@' and '..'.
	)
);
go

create table [product]
(
	[number]          integer identity not null primary key,
	[name]            varchar(100)     not null unique check (len([name]) >= 5),
	[description]     varchar(max)     not null,
	[category]        varchar(30)      not null references [Category] ([category])
		on delete no action
		on update no action,
	[subcategory]     varchar(30)      not null references [subcategory] ([subcategory])
		on delete no action
		on update no action,
	[price]           numeric(8, 2)    not null check ([price] > 0),
	[discountedPrice] numeric(8, 2)    null,
	[stock]           integer          null check ([stock] >= 0),
	[image]           varchar(1024)    not null,

	check ([price] > [discountedPrice])
);
go

create table [cart]
(
	[username] varchar(15) not null references [user] ([username]),
	[number]   integer     not null references [product] ([number]),
	[quantity] integer     not null check ([quantity] > 0),

	primary key ([username], [number])
);
go


--------------------------------
-- Insert values into tables. --
--------------------------------
insert [category] ([category], [translation]) values
	('Pastas', 'Pastas'),
	('Pizzas', 'Pizzas'),
	('Ice', 'IJs'),
	('Cake', 'Cake');
go

insert [subcategory] ([subcategory], [translation]) values
	('Vegetarian', 'Vegatarisch'),
	('With fish', 'Met vis'),
	('With meat', 'Met vlees'),
	('With fish and meat', 'Met vis en vlees'),
	('Sweet', 'Zoetigheid');
go

insert [user] ([username], [password], [firstName], [tussenvoegsel], [lastName], [street], [houseNumber], [postalAddress], [residence], [email], [sex], [phoneNumber])
values
	('admin',
	 'f5cbc3b5d2f2faf4015014e6df64f638c95cf1f0c948013d173bbe98ad41e9f9b7a8268f32f76c642327e89b1f523cb61a99aa4d3e353d78fd059338a7f86aae',
	 -- SHA512 has for 'adminsecret'.
	 'Gino',
	 'de',
	 'G swaggerboy',
	 'Hipsterlaan',
	 '1337',
	 '9001GG',
	 'Gansterstad',
	 'gino@gmail.com',
	 'M',
	 '1337009001');
go

/*----------------------------------
Can't use variables in insert statement?
Error: [S0002][155] 'varchar' is not a recognized CURSOR option.
declare [@category_pastas] varchar(15) = 'Pastas'
declare [@category_pizzas] varchar(15) = 'Pizzas'
declare [@stock] integer = 10
----------------------------------*/
insert [product] ([name], [description], [category], [subcategory], [price], [discountedPrice], [stock], [image]) values
	('Spaghetti Bolognese',
	 'Ambachtelijke spaghetti bolognese, gemaakt in een Chinese fabriek.',
	 'Pastas',
	 'With meat',
	 14.95,
	 null,
	 300,
	 'images/products/pasta_spaghetti_bolognese.jpg'),
	('Spaghetti met krab',
	 'Lekker, lekker, lekker. Dit gerecht is lekker.',
	 'Pastas',
	 'With meat',
	 19.95,
	 null,
	 30,
	 'images/products/pasta_spaghetti_crab.jpg'),
	('Spaghetti met gehaktballen',
	 'Met vlees uit eigen tuin. Ongelooflijk, we weten het. Wij <b>groeien</b> vlees!',
	 'Pastas',
	 'With meat',
	 17.50,
	 null,
	 15,
	 'images/products/pasta_spaghetti_meatballs.jpg'),
	('Pasta met groentes',
	 'Uit dezelfde tuin als het vlees!',
	 'Pastas',
	 'Vegetarian',
	 12.50,
	 null,
	 20,
	 'images/products/pasta_vegatables.jpg'),
	('Pizza met ansjovis',
	 'Heerlijke vis op een pizza.',
	 'Pizzas',
	 'With fish',
	 24.95,
	 19.95,
	 10,
	 'images/products/pizza_european_anchovy.jpg'),
	('Pizza Hawaii',
	 'Deze pizza is echt gemaakt in Hawaii, wow!',
	 'Pizzas',
	 'With meat',
	 15.95,
	 null,
	 5,
	 'images/products/pizza_hawaiian.jpg'),
	('Pizza Lahmacun',
	 'Deze pizza heeft echt een rare naam, wtf.',
	 'Pizzas',
	 'With meat',
	 22.50,
	 null,
	 10,
	 'images/products/pizza_lahmacun.jpg'),
	('Pizza Salami',
	 'De echte originele pizza met heerlijke gerookte salami.',
	 'Pizzas',
	 'With meat',
	 19.95,
	 null,
	 50,
	 'images/products/pizza_salami.jpg'),
	('Pizza Speciaal',
	 'Deze pizza is zo speciaal, met verassende ingredienten heerlijk bereid. Daarom is de prijs ook zo speciaal.',
	 'Pizzas',
	 'With fish and meat',
	 29.95,
	 24.95,
	 20,
	 'images/products/pizza_special.jpg'),
	('Bananen ijsje',
	 'We hebben ook ijs! Hmm!',
	 'Ice',
	 'Sweet',
	 15.98,
	 null,
	 20,
	 'images/products/banana_ice.jpg'
	),
	('Chocolade cake',
	 'Lekkere cake hmm!, Cake, CAKE!!!',
	 'Cake',
	 'Sweet',
	 19.98,
	 null,
	 15,
	 'images/products/choco_cake.jpg'),
	('Kaneel ijsje',
	 'ijs met kaneel! Kun je niet omheen!',
	 'Ice',
	 'Sweet',
	 19.98,
	 null,
	 15,
	 'images/products/cinnamon_ice.jpg'),
	('Pizza met Mozzarella en Tomaat',
	 'Met verse producten uit eigen geit!',
	 'Pizzas',
	 'Vegetarian',
	 19.79,
	 null,
	 20,
	 'images/products/pizza_mozzarella_tomato.jpg'),
	('Pasta met vlees',
	 'Met vers vlees',
	 'Pastas',
	 'With Meat',
	 1.00,
	 null,
	 20,
	 'images/products/pasta_beef.jpg'),
	('Pasta met kip',
	 'Met Verse kip, uit eigen tuin!',
	 'Pastas',
	 'With Meat',
	 45.00,
	 null,
	 15,
	 'images/products/pasta_chicken.jpg'),
	('Pasta met verse tomaat',
	 'Met verse tomaat uit eigen tuin',
	 'Pastas',
	 'Vegetarian',
	 9.00,
	 7.99,
	 15,
	 'images/products/pasta_fresh_tomato.jpg'),
	('Pizza met basilicum',
	 'Deze pizza heeft stukjes plant erop, walgelijk!',
	 'Pizzas',
	 'Vegetarian',
	 12.50,
	 null,
	 25,
	 'images/products/pizza_basilicum.jpg'),
	('Pizza honden kunst',
	 'Woef! Blaf! Woefwoef!<br/>Dit is <em>kunst</em>, koop <b>nu</b>!',
	 'Pizzas',
	 'With Meat',
	 449.95,
	 399.95,
	 1,
	 'images/products/pizza_dog.gif'),
	('Pizza Milano',
	 'Rechtstreeks uit Milaan!',
	 'Pizzas',
	 'Vegetarian',
	 19.99,
	 null,
	 10,
	 'images/products/pizza_milano.jpg'),
	('Pizza met zalm en kaas',
	 'Vis uit eigen tuin!',
	 'Pizzas',
	 'With Meat',
	 67.99,
	 null,
	 15,
	 'images/products/pizza_salamon_cheese.jpg'),
	('Stracciatella ijsje',
	 'Met chocolade van geitenmelk.',
	 'Ice',
	 'Sweet',
	 2.99,
	 null,
	 15,
	 'images/products/stracciatella_ice.jpg'),
	('Aardbeien ijsje',
	 'Met aardbeien uit de fabriek! <b>YUM!!!</b><br/>Omdat hij uit de fabriek komt is de voorraad zo hoog mensen!',
	 'Ice',
	 'Sweet',
	 1.49,
	 null,
	 1000,
	 'images/products/strawberry_ice.jpg'),
	('Tiramisu',
	 'Door grootmoeder klaargemaakt!<br/>In China.',
	 'Cake',
	 'Sweet',
	 4.59,
	 null,
	 15,
	 'images/products/tiramisu.jpg'),
	('Zuccotto cake',
	 'Geen idee wat er in gaat, maar, <b>YUM!!!!</b>',
	 'Cake',
	 'Sweet',
	 18.99,
	 null,
	 10,
	 'images/products/zuccotto_cake.jpg');
go