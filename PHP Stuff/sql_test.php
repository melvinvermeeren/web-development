<!DOCTYPE html>
<html>
<head>
	<title>SQL Test</title>
</head>
<body>
<?php
	$serverName = 'MELVIN-LAPTOP\SQLEXPRESS';
	$connectionOptions = array
	(
			'Database' => 'LorenzosPizza',
			'UID'      => 'sa',
			'PWD'      => 'root'
	);

	$conn = sqlsrv_connect($serverName, $connectionOptions);

	if ($conn === false)
	{
		?>
		<pre>
			<p style="color: red;">Connection failed!<br/>Information:</p>
			<?php print_r(sqlsrv_errors()); ?>
		</pre>
	<?php
	}
	else
	{
		?>
		<p style="color: green;">Connection established!</p>
		<?php
		sqlsrv_close($conn);
	}
?>
</body>
</html>