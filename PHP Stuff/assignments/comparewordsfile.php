<?php
	session_start();
	if (array_key_exists('reset', $_GET) && $_GET['reset'] == 'yes')
	{
		session_destroy();
		header('Location: comparewordsfile.php');
	}
	include 'comparewordsfile_functions.php';
	/**
	 * Created by IntelliJ IDEA.
	 * User: Melvin
	 * Date: 26/02/14
	 * Time: 14:51
	 */

	if (array_key_exists('tries', $_SESSION)) $_SESSION['tries']++;
	else $_SESSION['tries'] = 0;

	if (!array_key_exists('word', $_SESSION)) $_SESSION['word'] = getRandomWord();

	if (array_key_exists('currentInput', $_POST))
	{
		$currentInput = $_POST['currentInput'];
		$result       = compareWords($currentInput, $_SESSION['word']);

		if (strstr($result, ':', true) == 'Success')
		{
			$_SESSION['tries'] = 0;
			$_SESSION['word']  = getRandomWord();
		}
	}
	else $result = 'N/A';

	/******************************************************************/

	include '../subpages/html5_1.php'; ?>
<title>Comparewords File</title>
<style>
	table th
	{
		border           : 1px solid black;
		background-color : lightseagreen;
		color            : white;
	}

	table td
	{
		border : 1px solid black;
	}
</style>
<?php include '../subpages/html5_2.php'; ?>

<form action="comparewordsfile.php" method="post">
	<table>
		<tr>
			<th colspan="2">
				Length of the word:
				<?php echo(strlen($_SESSION['word'])); ?>
			</th>
		</tr>
		<tr>
			<td>
				Tries
			</td>
			<td>
				<input type="text" name="tries" value="<?php echo($_SESSION['tries']); ?>" readonly/>
			</td>
		</tr>
		<tr>
			<td>
				Try
			</td>
			<td>
				<input type="text" name="currentInput" placeholder="Type here..."/>
			</td>
		</tr>
		<tr>
			<td>
				Result
			</td>
			<td>
				<input type="text" name="result" value="<?php echo($result); ?>" readonly/>
			</td>
		</tr>
		<tr>
			<td>
				<input type="submit" value="Submit"/>
			</td>
			<td>
				<input type="reset" value="Reset" onclick="location.href='comparewordsfile.php?reset=yes'"/>
			</td>
		</tr>
	</table>
</form>

<?php include '../subpages/html5_3.php'; ?>
