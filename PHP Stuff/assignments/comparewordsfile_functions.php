<?php
	/**
	 * Created by IntelliJ IDEA.
	 * User: Melvin
	 * Date: 26/02/14
	 * Time: 15:14
	 */

	function compareWords($input, $word)
	{
		$return = "";

		if (strlen($input) != strlen($word)) return 'Length not the same';

		for ($i = 0; $i < strlen($input); $i++)
		{
			if ($input[$i] == $word[$i]) $return[$i] = $input[$i];
			else $return[$i] = ".";
		}

		if (implode('', $return) == $word) return 'Success: ' . implode('', $return);

		return implode('', $return);
	}

	function getRandomWord()
	{
		$words = file('comparewordsfile_words.txt');

		for ($i = 0; $i < sizeof($words); $i++) $words[$i] = rtrim($words[$i]);

		return $words[mt_rand(0, sizeof($words) - 1)];
	}