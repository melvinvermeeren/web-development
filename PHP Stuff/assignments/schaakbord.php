<?php include '../subpages/html5_1.php'; ?>
	<title>Schaakbord</title>
	<style>
		html
		{
			background-color : gray;
		}

		table
		{
			border-spacing : 0px;
		}

		table tr td
		{
			width   : 50px;
			height  : 50px;
			padding : 0px;
		}

		.table_white
		{
			background-color : white;
		}

		.table_black
		{
			background-color : black;
		}
	</style>
<?php include '../subpages/html5_2.php'; ?>

<?php
	/**
	 * Created by IntelliJ IDEA.
	 * User: Melvin
	 * Date: 19/02/14
	 * Time: 14:44
	 */
	echo('<table>');
	define("X_SIZE", 8);
	define("Y_SIZE", 8);
	$isBlack = true;

	for ($i = 0; $i < X_SIZE; $i++)
	{
		echo('<tr>');

		if (X_SIZE % 2 == 0) $isBlack = !$isBlack;

		for ($i2 = 0; $i2 < Y_SIZE; $i2++)
		{
			$isBlack = !$isBlack;

			echo('<td class="');

			if ($isBlack)
			{
				echo('table_black');
			}
			else
			{
				echo('table_white');
			}

			echo('"></td>');
		}

		echo('</tr>');
	}

	echo('</table>');

	include '../subpages/html5_3.php';

?>